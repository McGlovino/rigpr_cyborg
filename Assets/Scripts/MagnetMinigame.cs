﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Diagonalls collision raycast need to be longer
//move targets and cube further frommthe wall

public class MagnetMinigame : MonoBehaviour
{
    UIManager UIM;

    public bool startGame = false;
    public bool magObjGrabbed = false;
    float time = 240;
    float timePenalty = 0.15f;
    int additionalTime = 10;
    float scorePS = 0;
    float counter, totalCounter = 0;

    int startPoints = 1000;

    public List<Transform> targetPoints = new List<Transform>();
    public List<Transform> visitedTargets = new List<Transform>();

    Transform target;
    public Transform platform;

    // Vector3 targetScale = new Vector3(0.2f, 0.2f, 0.2f);
    Vector3 targetScale = new Vector3(0.2f, 0.2f, 0.2f);
    public Vector3 magObjStart, platformStart;

    public MagnetAttachment MA;

    //PUT ON CUBE
    void Start()
    {
        magObjStart = this.transform.position;
        UIM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<UIManager>();

        scorePS = startPoints / time;
        
        // Pick a random start point
        // move mag object there
        // select random taget - make sure isnt start
        // if collide with target select new position
    }

    void Update()
    {
        if (startGame && magObjGrabbed)
        {
            UIM.time = time - totalCounter;

            UIM.LM.magnetScore = (int)(scorePS * UIM.time);

            totalCounter += Time.deltaTime;

            if (UIM.time <= 0)
                EndGame();
        }

        RaycastWalls();
        //this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, targetPoints[0].position.z);
    }

    Transform PickRandomPoint()
    {
        return targetPoints[Random.Range(0, targetPoints.Count)];
    }

    public void SetUp()
    {
        startGame = true;

        //visitedTargets.Add(PickRandomPoint());
        //transform.position = visitedTargets[0].position;

        //transform.position += new Vector3(0,0.1f,0);

        while (target == null || visitedTargets.Contains(target))
            target = PickRandomPoint();
        

        target.GetComponent<MeshRenderer>().enabled = true;
        target.GetComponent<BoxCollider>().enabled = true;

        StartCoroutine(Grow());

        platform.position = this.transform.position + new Vector3(0,-0.2f,0.5f);
        platformStart = platform.position;
    }

    IEnumerator Grow()
    {
        this.GetComponent<Rigidbody>().useGravity = false;
        this.transform.position = magObjStart;
        platform.position = platformStart;
        this.GetComponent<Rigidbody>().velocity= new Vector3(0,0,0);
        while (Mathf.Abs(platform.position.z - this.transform.position.z) > 0.02f)
        {
            platform.position = Vector3.Lerp(platform.position, new Vector3(platform.position.x, platform.position.y, transform.position.z), 0.1f);
            yield return new WaitForSeconds(0.01f);
        }
        while (targetScale.x - transform.localScale.x > 0.02f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, 0.5f);
            yield return new WaitForSeconds(0.01f);
        }
        this.GetComponent<Rigidbody>().useGravity = true;
        this.GetComponent<SphereCollider>().enabled = true;
    }
    IEnumerator Shrink()
    {
        //turns collider off, so it cant be grabed
        this.GetComponent<SphereCollider>().enabled = false;
        this.GetComponent<Rigidbody>().useGravity = false;
        yield return StartCoroutine(PlatformShink());
        while (transform.localScale != new Vector3(0, 0, 0))
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0,0,0), 0.1f);
            yield return new WaitForSeconds(0.01f);
        }
        yield return StartCoroutine(Grow());
    }
    public IEnumerator PlatformShink()
    {
        while (Mathf.Abs(platform.position.z - platformStart.z) > 0.02f)
        {
            platform.position = Vector3.Lerp(platform.position, new Vector3(platformStart.x, platformStart.y, platformStart.z + 0.1f), 0.1f);
            yield return new WaitForSeconds(0.01f);
        }
    }
    void ChangeTarget()
    {
        MA.LetGo();
        StartCoroutine(Shrink());
        target.GetComponent<MeshRenderer>().enabled = false;
        target.GetComponent<BoxCollider>().enabled = false;
        visitedTargets.Add(target);

        if (visitedTargets.Count == targetPoints.Count)
        {
            EndGame();
            return;
        }

        while (visitedTargets.Contains(target))
            target = PickRandomPoint();

        target.GetComponent<MeshRenderer>().enabled = true;
        target.GetComponent<BoxCollider>().enabled = true;
    }

    void EndGame()
    {
        startGame = false;
        UIM.timeText.SetActive(false);
        StartCoroutine(UIM.UpdateTotalScore());
    }

    void RaycastWalls()
    {
        Debug.DrawRay(this.transform.position, this.transform.up * (transform.localScale.x / 2));
        RaycastHit hit;
        if (startGame &&
            ((Physics.Raycast(this.transform.position, this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")//Edges
            || (Physics.Raycast(this.transform.position, this.transform.forward, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")
            || (Physics.Raycast(this.transform.position, -1 * this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")
            || (Physics.Raycast(this.transform.position, -1 * this.transform.forward, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")
            || (Physics.Raycast(this.transform.position, this.transform.forward - this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")//Diagonals
            || (Physics.Raycast(this.transform.position, -1 * this.transform.forward - this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")
            || (Physics.Raycast(this.transform.position, -1 * this.transform.forward - -1 * this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")
            || (Physics.Raycast(this.transform.position, this.transform.forward - -1 * this.transform.up, out hit, transform.localScale.x / 1.9f) && hit.transform.tag == "Wall")))
        {
            time -= timePenalty;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        //if (startGame && collision.transform.tag == "Wall")
            //time -= timePenalty;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == target)
        {
            ChangeTarget();
            time += additionalTime;
        }
            
    }
}

