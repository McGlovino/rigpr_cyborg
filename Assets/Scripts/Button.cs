﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{

    public bool pressed = false;
    public bool playerTouch = false;
    bool descending = false;
    public GameObject trigger;
    public GameObject Base0;
    public GameObject Base1;
    public GameObject base2;
    public GameObject base3;
    public GameObject leftContoller;
    public GameObject rightController;

    Transform touched;
    float touchedStart;
    float changeInY;



    public Vector3 startPos;

    private void Start()
    {
        startPos = transform.position;
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), Base0.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), Base1.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), base2.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), base3.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), leftContoller.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), rightController.GetComponent<Collider>(), true);
    }

    private void Update()
    {
        if (playerTouch && transform.localPosition.y > 0.01f)
        {
            changeInY = touchedStart - touched.position.y;
            this.transform.position = new Vector3(transform.position.x, startPos.y - changeInY, transform.position.z);
        }


        if (transform.position.y < startPos.y && !playerTouch && !descending)
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.0025f, transform.position.z);
        else if (transform.position.y > startPos.y)
            transform.position = startPos;
        //if (transform.localPosition.y < 0.01f)
            //transform.localPosition = new Vector3(transform.localPosition.x, 0.01f, transform.localPosition.z);
    }
    
    void OnTriggerEnter(Collider collision) 
    {
        if (collision.gameObject == trigger)
        {
            pressed = true;
        }
        if (collision.gameObject.tag == "Player")    
        {   
            touched = collision.transform;
            touchedStart = touched.position.y;
            playerTouch = true;
        }

    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject == trigger)
            pressed = false;

        if (collision.gameObject.tag == "Player")
            playerTouch = false;
    }

    public void Destroy()
    {
        StartCoroutine(Descend());
    }

    IEnumerator Descend()
    {
        descending = true;
        while (this.transform.parent.position.y >= -0.5f)
        {
            this.transform.parent.position = new Vector3(this.transform.parent.position.x, (this.transform.parent.position.y - 0.015f), this.transform.parent.position.z);
            yield return new WaitForSeconds(0.0025f);
        }
        this.transform.parent.gameObject.SetActive(false);
    }
}