﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Vector3 moveDirection;
    public SpawnTargets ST;
    public int damage;
    public LeaderboardManager LM;
    float bulletSpeed = 25f;

    private void Start()
    {
    //GA = GameObject.Find("[CameraRig]").GetComponent<GunAttachment>();
        //LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();
        //ST = GameObject.FindGameObjectWithTag("SpawnTargets").GetComponent<SpawnTargets>();
        StartCoroutine( DestroyAfter());
    }

    public void MoveDirection(Vector3 direction)
    {
        moveDirection = direction;
        moveDirection = moveDirection.normalized;
    }

    void Update()
    {
        transform.position = transform.position + (moveDirection*(bulletSpeed *Time.deltaTime));
    }


    void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.GetComponent<Target>() != null)
        {
            Target temp = ST.FindTarget(collision.transform.gameObject);
            temp.TakeDamage(damage);
        }
        if (collision.gameObject.GetComponent<Fruit>() != null)
            collision.gameObject.GetComponent<Fruit>().Destroy();

        if (collision.tag != "Player")
        {
            Debug.Log(collision.name);
            Destroy(this.gameObject);
        }

    }

    IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}

