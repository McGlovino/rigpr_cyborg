﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DitzelGames.FastIK
{

    public class IK2 : MonoBehaviour
    {

        public int ChainLength = 2;

        public Transform target;
        public Transform Pole;

        public int Iterations = 10;

        public float Delta = 0.001f;

        [Range(0, 1)]
        public float SnapBackStrength = 1f;

        public bool stiffWrist = false;


        protected float[] bonesLength;
        protected float completeLength;
        protected Transform[] bones;
        protected Vector3[] positions;
        protected Vector3[] startDirection;
        protected Quaternion[] startRotationBone;
        protected Quaternion startRotationtarget;
        protected Transform root;

        void Awake()
        {
            Init();
        }

        void Init()
        {
            //initial array
            bones = new Transform[ChainLength + 1];
            positions = new Vector3[ChainLength + 1];
            bonesLength = new float[ChainLength];
            startDirection = new Vector3[ChainLength + 1];
            startRotationBone = new Quaternion[ChainLength + 1];

            //find root
            root = transform;
            for (var i = 0; i <= ChainLength; i++)
            {
                if (root != null)
                    root = root.parent;
            }

            startRotationtarget = GetRotationInRootSpace(target);


            //init data
            var current = transform;
            completeLength = 0;
            for (var i = bones.Length - 1; i >= 0; i--)
            {
                bones[i] = current;
                startRotationBone[i] = GetRotationInRootSpace(current);

                if (i == bones.Length - 1)
                {
                    startDirection[i] = GetPositionInRootSpace(target) - GetPositionInRootSpace(current);
                }
                else if (i == bones.Length - 1 && stiffWrist)
                {
                    startDirection[i] = target.position - current.position;
                    bonesLength[i] = startDirection[i].magnitude;
                    completeLength += bonesLength[i];
                }
                else
                {
                    //mid bone
                    startDirection[i] = GetPositionInRootSpace(bones[i + 1]) - GetPositionInRootSpace(current);
                    bonesLength[i] = startDirection[i].magnitude;
                    completeLength += bonesLength[i];
                }
                current = current.parent;
            }
        }

        void LateUpdate()
        {
            ResolveIK();
        }

        private void ResolveIK()
        {
            if (target == null)
                return;

            if (bonesLength.Length != ChainLength)
                Init();

            //get position
            for (int i = 0; i < bones.Length; i++)
                positions[i] = GetPositionInRootSpace(bones[i]);

            var targetPosition = GetPositionInRootSpace(target);
            var targetRotation = GetRotationInRootSpace(target);

            if ((targetPosition - GetPositionInRootSpace(bones[0])).sqrMagnitude >= (completeLength * completeLength)+1)
            {
                var direction = (targetPosition - positions[0]).normalized;
                //set everything after root
                for (int i = 1; i < positions.Length; i++)
                    positions[i] = positions[i - 1] + direction * bonesLength[i - 1];
            }
            else
            {
                for (int i = 0; i < positions.Length - 1; i++)
                    positions[i + 1] = Vector3.Lerp(positions[i + 1], positions[i] + startDirection[i], SnapBackStrength);

                for (int iteration = 0; iteration < Iterations; iteration++)
                {
                    //back
                    for (int i = positions.Length - 1; i > 0; i--)
                    {
                        //set it to target
                        if (i == positions.Length - 1)
                            positions[i] = targetPosition;
                        else //set in line on distance
                            positions[i] = positions[i + 1] + (positions[i] - positions[i + 1]).normalized * bonesLength[i];
                    }

                    //forward
                    for (int i = 1; i < positions.Length; i++)
                        positions[i] = positions[i - 1] + (positions[i] - positions[i - 1]).normalized * bonesLength[i - 1];

                    //if its close enough
                    if ((positions[positions.Length - 1] - targetPosition).sqrMagnitude < Delta * Delta)
                        break;
                }
            }

            //move elbow towards pole
            if (Pole != null)
            {
                var polePosition = GetPositionInRootSpace(Pole);
                for (int i = 1; i < positions.Length - 1; i++)
                {
                    var plane = new Plane(positions[i + 1] - positions[i - 1], positions[i - 1]);
                    var projectedPole = plane.ClosestPointOnPlane(polePosition);
                    var projectedBone = plane.ClosestPointOnPlane(positions[i]);
                    var angle = Vector3.SignedAngle(projectedBone - positions[i - 1], projectedPole - positions[i - 1], plane.normal);
                    positions[i] = Quaternion.AngleAxis(angle, plane.normal) * (positions[i] - positions[i - 1]) + positions[i - 1];
                }
            }

            //set position & rotation
            for (int i = 0; i < positions.Length; i++)
            {
                if (i == positions.Length - 1)
                    SetRotationInRootSpace(bones[i], Quaternion.Inverse(targetRotation) * startRotationtarget * Quaternion.Inverse(startRotationBone[i]));
                else
                    SetRotationInRootSpace(bones[i], Quaternion.FromToRotation(startDirection[i], positions[i + 1] - positions[i]) * Quaternion.Inverse(startRotationBone[i]));
                SetPositionInRootSpace(bones[i], positions[i]);
            }
        }

        private Vector3 GetPositionInRootSpace(Transform current)
        {
            if (root == null)
                return current.position;
            else
                return Quaternion.Inverse(root.rotation) * (current.position - root.position);
        }

        private void SetPositionInRootSpace(Transform current, Vector3 position)
        {
            if (root == null)
                current.position = position;
            else
                current.position = root.rotation * position + root.position;
        }

        private Quaternion GetRotationInRootSpace(Transform current)
        {
            if (root == null)
                return current.rotation;
            else
                return Quaternion.Inverse(current.rotation) * root.rotation;
        }

        private void SetRotationInRootSpace(Transform current, Quaternion rotation)
        {
            if (root == null)
                current.rotation = rotation;
            else
                current.rotation = root.rotation * rotation;
        }
    }
}