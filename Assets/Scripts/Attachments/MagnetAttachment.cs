﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;

public class MagnetAttachment : MonoBehaviour
{
    public GameObject rightHand;
    public GameObject rightHandBlackHole;
    public GameObject rightHandElectromagnet;
    public ControllsManager CM;
    GameObject aim;
    public GameObject aimPrefab;
    public GameObject objInHand;
    public bool grabbed, grabbedGravity;
    Transform grabbedOrigionalParent;
    LayerMask wallLayerMask;
    float speed = 10f;
    public SpawnTargets ST;

    public MagnetMinigame MM;

    void Start()
    {
        wallLayerMask = LayerMask.GetMask("player", "InvisableWall");
        wallLayerMask = ~wallLayerMask;
        grabbed = false;
        aim = Instantiate(aimPrefab);
        aim.SetActive(false);

        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();
    }
    void FixedUpdate()
    {
        if (grabbed && !grabbedGravity && objInHand == null)
            LetGo();

        if (grabbed)
            objInHand.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        if (CM.Trigger_Touch.GetState(CM.rightController))
        {
            if (!aim.activeSelf)
                aim.SetActive(true);
            showAim(CM.currentAtachment.isPalm);
        }
        else
            aim.SetActive(false);

        //showLaser(leftPose);
        
        if (CM.Trigger_Pull.GetAxis(CM.rightController) > 0.9f)
        {
            RaycastHit hit;
            //if (grabbed == false)// stops multiple things bieng grabbed. But stops raytrace. turn into a boolean
            //{
            if (Physics.Raycast(rightHand.transform.position, rightHand.transform.forward, out hit, 100, wallLayerMask))
            {
                if ( hit.transform.tag == "Grabable" && grabbed == false)
                {
                    grabbed = true;
                    objInHand = hit.transform.gameObject;
                    grabbedOrigionalParent = objInHand.transform.parent;
                    objInHand.transform.parent = rightHand.transform;
                    if (objInHand.GetComponent<Rigidbody>().useGravity)
                    {
                        grabbedGravity = true;
                        objInHand.GetComponent<Rigidbody>().useGravity = false;
                    }
                    else
                        grabbedGravity = false;
                }
                else if (hit.transform.tag == "Destroyable" && grabbed == false)
                {
                    grabbed = true;
                    objInHand = hit.transform.gameObject;
                    grabbedOrigionalParent = objInHand.transform.parent;
                    objInHand.transform.parent = rightHand.transform;
                    if (objInHand.GetComponent<Rigidbody>().useGravity)
                    {
                        grabbedGravity = true;
                        objInHand.GetComponent<Rigidbody>().useGravity = false;
                    }
                    else
                    {
                        grabbedGravity = false;
                        //Specifically for droids
                        ST.FindTarget(objInHand).grabbed = true;
                    }
                    if (objInHand.gameObject.GetComponent<Fruit>() != null)
                    {
                        objInHand.GetComponent<Fruit>().grabbed = true;
                    }
                }
                else if (hit.transform.tag == "MagObject" && !grabbed)
                {
                    grabbed = true;
                    grabbedGravity = true;
                    objInHand = hit.transform.gameObject;
                    //objInHand.GetComponent<Rigidbody>().useGravity = true;
                }
                //if the laser isnt hitting the object
                //work out the closest point to raytrace line
                //move to that point using velocity movement
                //check doesnt bug out with slider movement, if it does change slider movement to velocity movement
                else if (grabbed && hit.transform.gameObject != objInHand)
                {
                    Vector3 closestPoint = ProjectPointOnLine(rightHand.transform.position, (rightHand.transform.position - hit.point).normalized, objInHand.transform.position);
                    objInHand.GetComponent<Rigidbody>().velocity = (closestPoint - objInHand.transform.position).normalized * 500f * Time.deltaTime;
                }

                if (grabbed && objInHand.tag == "MagObject")
                {
                    //to solve jitter
                    //only moves if a distance away from line
                    Vector3 closestPoint = ProjectPointOnLine(rightHand.transform.position, (rightHand.transform.position - hit.point).normalized, objInHand.transform.position);
                    if (Mathf.Abs((closestPoint - objInHand.transform.position).magnitude) > 0.1f)
                    {
                        Vector3 targetDestination = new Vector3(hit.point.x, hit.point.y, MM.targetPoints[0].position.z);
                        objInHand.GetComponent<Rigidbody>().velocity = ((targetDestination - objInHand.transform.position).normalized * 750 * Time.deltaTime);
                        StartCoroutine(MM.PlatformShink());
                        MM.magObjGrabbed = true;
                        grabbedGravity = true;
                        objInHand.GetComponent<Rigidbody>().useGravity = false;
                    }
                }
            }
        }

        if (CM.Trigger_Pull.GetAxis(CM.rightController) < 0.9f)
        {
             LetGo();
        }

        if (grabbed == true && CM.Touchpad.GetActive(CM.rightController))// && CM.Touchpad.GetAxis(CM.rightController) != new Vector2(0, 0)
        {
            Vector2 touchPadVector = CM.Touchpad.GetAxis(CM.rightController);

           // if (touchPadVector.y < 0.1f && touchPadVector.y > -0.1f)
           //     touchPadVector.y = 0;

            Vector3 directionToPlayer = objInHand.transform.position - rightHand.transform.position;
            if(!(directionToPlayer.magnitude < 0.5f && touchPadVector.y < 0f))
             objInHand.transform.position = objInHand.transform.position + (directionToPlayer.normalized * touchPadVector.y * Time.deltaTime * speed);
        }
    }

    public void LetGo()
    {
        if (objInHand != null)
        {
            objInHand.transform.parent = null;
            if (grabbedGravity)
                objInHand.GetComponent<Rigidbody>().useGravity = true;
            else //We know its a target if gravity is false
                ST.FindTarget(objInHand).grabbed = false;
            objInHand.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        objInHand = null;
        grabbed = false;
    }

    void showAim(bool isPalm)
    {
        LineRenderer lr = aim.GetComponent<LineRenderer>();
        RaycastHit hit;
        lr.SetPosition(0, rightHand.transform.position);
        if (Physics.Raycast(rightHand.transform.position, rightHand.transform.forward, out hit, 100, wallLayerMask))
        {

            if (hit.collider.tag == "Destroyable" && hit.collider.gameObject.GetComponent<Target>() != null) //Show healh when hovering over a droid
            {
                hit.collider.gameObject.GetComponent<Target>().HB.isHover = true;
            }

            if (grabbed)
            {
                //float distance = (objInHand.transform.position - rightHand.transform.position).magnitude;
                //lr.SetPosition(1, isPalm ? (rightHand.transform.right * -1) : ((rightHand.transform.up * -1) + rightHand.transform.forward) * 5000);
                lr.SetPosition(1, hit.point);
            }
            else if (hit.collider)
                lr.SetPosition(1, hit.point);
        }
        else
            lr.SetPosition(1, rightHand.transform.forward * 5000);
    }


    public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
    {

        //get vector from point on line to point in space
        Vector3 linePointToPoint = point - linePoint;

        float t = Vector3.Dot(linePointToPoint, lineVec);

        return linePoint + lineVec * t;
    }
    public void DisableAttachment()
    {
        aim.SetActive(false);
        this.enabled = false;
    }
}
