﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;

public class GunAttachment : MonoBehaviour
{

    public GameObject rightHand;
    public GameObject rightHandCannon;
    public GameObject rightHandMinigun;
    public ControllsManager CM;
    public LeaderboardManager LM;
    public GameObject LaserPrefab; //referemce the laser prefab
    GameObject aim;
    GameObject laser;
    public GameObject aimPrefab;
    public GameObject bulletPrefab;
    public GameObject bulletSmallPrefab;

    //For creating sound
    public AudioSource laserSound;
    public AudioSource bulletSound;


    public SpawnTargets ST;

    float waitTime = 0;
    void Start()
    {
        aim = Instantiate(aimPrefab);
        aim.SetActive(false);

        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();
        LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();
    }
    void Update()
    {
        if (CM.Trigger_Touch.GetState(CM.rightController))
        {
            if (!aim.activeSelf)
                aim.SetActive(true);
            showAim(CM.currentAtachment.name);
        }
        else
        {
            aim.SetActive(false);
        }

        waitTime += Time.deltaTime;
        float timeToWait = 0;
        if (CM.currentAtachment.name == "laser")
            timeToWait = 0.075f;
        else if (CM.currentAtachment.name == "cannon")
            timeToWait = 0.2f;
        else if (CM.currentAtachment.name == "minigun")
            timeToWait = 0.05f;
        if (CM.Trigger_Pull.GetAxis(CM.rightController) > 0.9f && waitTime > timeToWait)
        {
            waitTime = 0;

            Vector3 handPosition = new Vector3();
            if (CM.currentAtachment.name == "laser")
                handPosition = rightHand.transform.position;
            else if (CM.currentAtachment.name == "cannon")
                handPosition = rightHandCannon.transform.position;
            else if (CM.currentAtachment.name == "minigun")
                handPosition = rightHandMinigun.transform.position;
            RaycastHit hit;
            if (Physics.Raycast(handPosition
                , (CM.currentAtachment.isPalm ? (-rightHand.transform.right) : (rightHandCannon.transform.right))
                , out hit, 100, LayerMask.GetMask("Default", "Floor")))
            {
                if (CM.currentAtachment.name == "laser")
                {
                    laserSound.Play();
                    StartCoroutine(showLaser(hit));
                    if (hit.collider.gameObject.GetComponent<Target>() != null) {
                        Target temp = ST.FindTarget(hit.transform.gameObject);
                        temp.TakeDamage(CM.currentAtachment.damage);
                    }
                    if (hit.collider.gameObject.GetComponent<Fruit>() != null)
                        hit.collider.gameObject.GetComponent<Fruit>().Destroy();
                }
                else if (CM.currentAtachment.name == "cannon") {
                    bulletSound.Play();
                    GameObject bulletTemp = Instantiate(bulletPrefab, rightHandCannon.transform.position, transform.rotation);
                    bulletTemp.transform.rotation = Quaternion.LookRotation(rightHandCannon.transform.right);
                    bulletTemp.GetComponent<Bullet>().MoveDirection((rightHandCannon.transform.right));
                    bulletTemp.GetComponent<Bullet>().damage = CM.currentAtachment.damage;
                    bulletTemp.GetComponent<Bullet>().ST = ST;
                    bulletTemp.GetComponent<Bullet>().LM = LM;
                }
                else if (CM.currentAtachment.name == "minigun")
                {
                    bulletSound.Play();
                    GameObject bulletTemp = Instantiate(bulletSmallPrefab, rightHandMinigun.transform.position, transform.rotation);
                    bulletTemp.transform.rotation = Quaternion.LookRotation(rightHandMinigun.transform.right);
                    bulletTemp.GetComponent<Bullet>().MoveDirection((rightHandMinigun.transform.right));
                    bulletTemp.GetComponent<Bullet>().damage = CM.currentAtachment.damage;
                    bulletTemp.GetComponent<Bullet>().ST = ST;
                    bulletTemp.GetComponent<Bullet>().LM = LM;
                }
            }
            else
            {
                if (CM.currentAtachment.name == "laser")
                {
                    laserSound.Play();
                    StartCoroutine(showLaser());
                }
                else if (CM.currentAtachment.name == "cannon")
                {
                    bulletSound.Play();
                    GameObject bulletTemp = Instantiate(bulletPrefab, rightHandCannon.transform.position, transform.rotation);
                    bulletTemp.transform.rotation = Quaternion.LookRotation(rightHandCannon.transform.right);
                    bulletTemp.GetComponent<Bullet>().MoveDirection((rightHandCannon.transform.right));
                    bulletTemp.GetComponent<Bullet>().damage = CM.currentAtachment.damage;
                    bulletTemp.GetComponent<Bullet>().ST = ST;
                    bulletTemp.GetComponent<Bullet>().LM = LM;
                }
                else if (CM.currentAtachment.name == "minigun")
                {
                    bulletSound.Play();
                    GameObject bulletTemp = Instantiate(bulletSmallPrefab, rightHandMinigun.transform.position, transform.rotation);
                    bulletTemp.transform.rotation = Quaternion.LookRotation(rightHandMinigun.transform.right);
                    bulletTemp.GetComponent<Bullet>().MoveDirection((rightHandMinigun.transform.right));
                    bulletTemp.GetComponent<Bullet>().damage = CM.currentAtachment.damage;
                    bulletTemp.GetComponent<Bullet>().ST = ST;
                    bulletTemp.GetComponent<Bullet>().LM = LM;
                }
            }
        }
    }
    IEnumerator showLaser(RaycastHit hit)
    {
        Destroy(laser);
        laser = Instantiate(LaserPrefab);

        LineRenderer lr = laser.GetComponent<LineRenderer>();
        lr.SetPosition(0, rightHand.transform.position);
        if (hit.collider)
            lr.SetPosition(1, hit.point);

        yield return new WaitForSeconds(0.1f);
        Destroy(laser);
    }
    IEnumerator showLaser()
    {
        Destroy(laser);
        laser = Instantiate(LaserPrefab);

        LineRenderer lr = laser.GetComponent<LineRenderer>();
        lr.SetPosition(0, rightHand.transform.position);

        lr.SetPosition(1, -rightHand.transform.right * 5000);

        yield return new WaitForSeconds(0.1f);
        Destroy(laser);
    }
    void showAim(string name)
    {
        LineRenderer lr = aim.GetComponent<LineRenderer>();
        RaycastHit hit;
        if(name == "laser")
        {
            lr.SetPosition(0, rightHand.transform.position);
            if (Physics.Raycast(rightHand.transform.position,
               rightHand.transform.right * -1, out hit, 100, LayerMask.GetMask("Default", "Floor")))
            {
                if (hit.collider)
                    lr.SetPosition(1, hit.point);

                if (hit.collider.gameObject.GetComponent<Target>() != null)
                    hit.collider.gameObject.GetComponent<Target>().HB.isHover = true;
            }
            else lr.SetPosition(1, (rightHand.transform.right * -1) * 5000);
        }
        else if (name == "cannon")
        {
            lr.SetPosition(0, rightHandCannon.transform.position);
            if (Physics.Raycast(rightHandCannon.transform.position,
               rightHandCannon.transform.right, out hit, 100, LayerMask.GetMask("Default", "Floor")))
            {
                if (hit.collider)
                    lr.SetPosition(1, hit.point);

                if (hit.collider.gameObject.GetComponent<Target>() != null)
                    hit.collider.gameObject.GetComponent<Target>().HB.isHover = true;
            }
            else lr.SetPosition(1, (rightHandCannon.transform.right) * 5000);
        }
        else if (name == "minigun")
        {
            lr.SetPosition(0, rightHandMinigun.transform.position);
            if (Physics.Raycast(rightHandMinigun.transform.position,
               rightHandMinigun.transform.right, out hit, 100, LayerMask.GetMask("Default", "Floor")))
            {
                if (hit.collider)
                    lr.SetPosition(1, hit.point);

                if (hit.collider.gameObject.GetComponent<Target>() != null)
                    hit.collider.gameObject.GetComponent<Target>().HB.isHover = true;
            }
            else lr.SetPosition(1, (rightHandMinigun.transform.right) * 5000);
        }

        /*lr.SetPosition(0, isLaser ? rightHand.transform.position : rightHandCannon.transform.position);
        if (Physics.Raycast(isLaser ? rightHand.transform.position : rightHandCannon.transform.position,
            isLaser ? (rightHand.transform.right * -1) : (rightHandCannon.transform.right), out hit, 100, LayerMask.GetMask("Default", "Floor")))
        {
            if (hit.collider)
                lr.SetPosition(1, hit.point);

            if (hit.collider.gameObject.GetComponent<Target>() != null)
                hit.collider.gameObject.GetComponent<Target>().HB.isHover = true;
        }
        else lr.SetPosition(1, isLaser ? (rightHand.transform.right * -1) * 5000 : ((rightHandCannon.transform.right) * 5000));*/
    }

    public void DisableAttachment()
    {
        if(aim != null)
            aim.SetActive(false);
        this.enabled = false;
    }

}
