﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootText : MonoBehaviour
{
    List<string> bootTextText;
    UIManager UIM;

    void Start()
    {
        UIM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<UIManager>();
    }

    private IEnumerator BootTextUpdate()
    {
        for (int i = 0; i < bootTextText.Count; i++)
        {
            yield return new WaitForSeconds(0.05f);
            this.GetComponent<TextMesh>().text = bootTextText[i];
        }
        UIM.OpenKeyboard();
        UIM.CloseBootText();
    }
    public void PopulateBootText()
    {
        bootTextText = new List<string>();

        for (int i = 0; i <= 100; i++)
            bootTextText.Add("BOOTING: " + i + "% complete");
        for (int i = 0; i <= 10; i++)
            bootTextText.Add("BOOTING: SUCCESSFUL");

        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK:");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping:");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1 \n" +
            "Getting ip:");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1 \n" +
            "Getting ip: 149.187.246.24");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1 \n" +
            "Getting ip: 149.187.246.24 \n" +
            "Connecting to Other Devices on Network:");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1 \n" +
            "Getting ip: 149.187.246.24 \n" +
            "Connecting to Other Devices: seccessful");
        for (int i = 0; i <= 2; i++)
            bootTextText.Add("CONNECING TO NETWORK: \n" +
            "Finding Server: successful \n" +
            "Testing Ping: 1 \n" +
            "Getting ip: 149.187.246.24 \n" +
            "Connecting to Other Devices: seccessful \n" +
            "69 Devices Found on Network");
        for (int i = 0; i <= 5; i++)
            bootTextText.Add("NETWORK INITIALIZATION COMPLETE");

        for (int i = 0; i <= 10; i++)
            bootTextText.Add("LAUCHING 'EYE'OS");
        for (int i = 0; i <= 25; i++)
            bootTextText.Add("Welcome.");


        StartCoroutine(BootTextUpdate());
    }
}
