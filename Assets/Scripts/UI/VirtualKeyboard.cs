﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VirtualKeyboard : MonoBehaviour
{
    ControllsManager CM;
    UIManager UIM;
    LeaderboardManager LM;

    Color blueTwo = new Color(0.3f, 0.9f, 0.9f, 0.9f);
    Color white = new Color(1f, 1f, 1f, 0.8f);

    bool changeWait = false;

    public GameObject[] rowOne;
    public GameObject[] rowTwo;
    public GameObject[] rowThree;
    GameObject selected;
    public GameObject cantBeEmpty;

    public GameObject nameBox;

    string playerName;
    string playerNameUnderline;

    int row, collumn, keyboardCursor;

    void Start()
    {
        UIM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<UIManager>();
        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();
        LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();

        selected = rowOne[0];
        selected.GetComponent<Image>().color = blueTwo;

        playerName = "";

        row = 0;
        collumn = 0;
        keyboardCursor = 0;
    }

    void Update()
    {
        if (CM.Jostick_Click.GetLastStateDown(CM.rightController))
        {
            UIM.OpenInstructions();
            UIM.CloseKeyboard();
        }

        if (CM.Jostick_Position.GetAxis(CM.leftController) != new Vector2(0, 0) && changeWait == false)
        {
            //Change old one back to white
            selected.GetComponent<Image>().color = white;

            var joystickVector = CM.Jostick_Position.GetAxis(CM.leftController);
            //Change selection
            if (Mathf.Abs(joystickVector.x) > Mathf.Abs(joystickVector.y))
            {
                if (joystickVector.x > 0.2f)
                {
                    collumn++;
                    if (collumn > 9)
                        collumn = 0;
                }
                else if (joystickVector.x < -0.2f)
                {
                    collumn--;
                    if (collumn < 0)
                        collumn = 9;
                }
            }
            else {
                if (joystickVector.y > 0.2f)
                {
                    row--;
                    if (row < 0)
                        row = 2;
                }
                else if (joystickVector.y < -0.2f)
                {
                    row++;
                    if (row > 2)
                        row = 0;
                }
            }
            selected = GetSelected(row,collumn);
            //change new one to blue
            selected.GetComponent<Image>().color = blueTwo;

            changeWait = true;
            StartCoroutine(ChangeWait());
        }

        if (CM.A_Button.GetLastStateDown(CM.anyController))
        {
            if (row == 1 && collumn == 9)
            { //BackSpace
                playerName = playerName.Remove(keyboardCursor - 1, 1);
                keyboardCursor--;
            }
            else if (row == 2 && collumn == 7)
                keyboardCursor--;
            else if (row == 2 && collumn == 8)
                keyboardCursor++;
            else if (row == 2 && collumn == 9) //Done
            {
                //send name to leaderboard
                //enable arm mounted menu
                //disable self

                if (playerName.Length > 0)
                {
                    LM.playerName = playerName;
                    LM.UpdateLeaderboard(0);
                    GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().OpenDoors();
                    UIM.OpenAMM();
                    UIM.IM.ChangeInstruction(1); //Change instrucion pannel to movement.
                    UIM.IM.OpenPanel(); //Open movement instructions.
                    UIM.CloseKeyboard();                   
                }
                else
                    StartCoroutine(CantBeEmpty()); 
            }
            else
            {
                if (playerName != null && playerName.Length < 16)
                {
                    int tempLength = playerName.Length;
                    string playerNameTemp = "";
                    for (int i = 0; i <= tempLength; i++)
                    {
                        if (i == keyboardCursor)
                            playerNameTemp += selected.GetComponentInChildren<Text>().text;
                        else if (i > keyboardCursor && keyboardCursor != 0)
                            playerNameTemp += playerName[i - 1];
                        else
                            playerNameTemp += playerName[i];
                    }
                    playerName = playerNameTemp;
                    keyboardCursor++;
                }
                else if (playerName == null)
                {
                    playerName += selected.GetComponentInChildren<Text>().text;
                    keyboardCursor++;
                }
            }
            if (keyboardCursor < 1)
                keyboardCursor = 1;
            if (keyboardCursor > playerName.Length)
                keyboardCursor = playerName.Length;

            //Highlight letter
            playerNameUnderline = "";
            for (int i = 0; i < playerName.Length; i++)
            {
                if (i == keyboardCursor-1)
                    playerNameUnderline += "<color=#5390B9>" + playerName[i] + "</color>";
                else
                    playerNameUnderline += playerName[i];
            }

            nameBox.GetComponentInChildren<Text>().text = playerNameUnderline;
        }
    }

    GameObject GetSelected(int r, int c)
    {
        switch (r){
            case 0:
                return rowOne[c];
            case 1:
                return rowTwo[c];
            case 2:
                return rowThree[c];
            default:
                return null;
        }
    }

    IEnumerator ChangeWait()
    {
        yield return new WaitForSeconds(0.175f);
        changeWait = false;
    }

    IEnumerator CantBeEmpty()
    {
        cantBeEmpty.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        cantBeEmpty.SetActive(false);
    }
}
