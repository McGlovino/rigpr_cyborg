﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] instructions;
    public GameObject currentInstructionPanel;
    public int currentInstruction = 0; // 0 = Keyboard, 1 = Movement, 2 = laser, 3 = cannon, 4 = magnet

    public ControllsManager CM;
    public UIManager UIM;
    public void Setup()
    {
        currentInstruction = 0;
        currentInstructionPanel = instructions[currentInstruction];
        currentInstructionPanel.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        if (CM.B_Button.GetLastStateDown(CM.anyController) || CM.A_Button.GetLastStateDown(CM.anyController))
        {
            if(currentInstruction == 0)
                UIM.OpenKeyboard();

            ClosePanel();            
        }
    }

    public void ChangeInstruction(int currentInstructioni)
    {
        ClosePanel();
        currentInstruction = currentInstructioni;
        currentInstructionPanel = instructions[currentInstruction];
    }

    public void OpenPanel()
    {
        currentInstructionPanel.SetActive(true);
    }

    public void ClosePanel()
    {
        currentInstructionPanel.SetActive(false);
    }

}
