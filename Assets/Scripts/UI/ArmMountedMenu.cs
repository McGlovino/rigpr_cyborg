﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArmMountedMenu : MonoBehaviour
{
    //public GameObject rightHand;
    public ControllsManager CM;
    public InstructionsManager IM;

    public List<GameObject> items;
    //GameObject selectedPrev;
    GameObject selected;
    int selectedI;

    float changeWaitTime = 0;


    Color blueOne = new Color(0.08f, 0.4f, 0.5f, 0.8f);
    Color blueTwo = new Color(0.3f, 0.9f, 0.9f, 0.8f);
    Color white = new Color(1f, 1f, 1f, 0.4f);
    Color Red = new Color(0.94f, 0.32f, 0.32f, 0.6f);

    void Start()
    {
        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();

        selected = items[0];
        selectedI = 0;
        selected.GetComponent<Image>().color = blueTwo;

    }

    // Update is called once per frame
    void Update()
    {
        //if(selectedPrev != null)
        /*foreach(GameObject g in items)
        {
            if(g != selected)
                g.GetComponent<Image>().color = Color.Lerp(selected.GetComponent<Image>().color, white, 0.5f);
            else
                g.GetComponent<Image>().color = Color.Lerp(selected.GetComponent<Image>().color, blueTwo, 0.2f);

        }*/

        if (CM.Jostick_Position.GetAxis(CM.leftController) != new Vector2(0, 0) && changeWaitTime > 0.2f)
        {

            var joystickVector = CM.Jostick_Position.GetAxis(CM.leftController);

            if (joystickVector.x > 0.2f)
            {
                if (selectedI == items.Count - 1)
                    selectedI = 0;
                else
                    selectedI++;

                changeWaitTime = 0;
                selected.GetComponent<Image>().color = white;
                selected = items[selectedI];
                selected.GetComponent<Image>().color = blueTwo;
            }
            else if (joystickVector.x < -0.2f) {
                if (selectedI == 0)
                    selectedI = items.Count - 1;
                else
                    selectedI--;

                changeWaitTime = 0;
                selected.GetComponent<Image>().color = white;
                selected = items[selectedI];
                selected.GetComponent<Image>().color = blueTwo;
            }
        }

        changeWaitTime += Time.deltaTime;

        switch (selectedI)
        {
            case 0:
                IM.ChangeInstruction(2); //Change to laser instructions
                IM.OpenPanel();  //open new pannel
                break;
            case 1:
                IM.ChangeInstruction(3); //Change to cannon instructions
                IM.OpenPanel();  //open new pannel
                break;
            case 2:
                IM.ChangeInstruction(6); //Change to minigun instructions
                IM.OpenPanel();  //open new pannel
                break;
            case 3:
                IM.ChangeInstruction(4); //Change to magnet instructions
                IM.OpenPanel();  //open new pannel
                break;
            case 4:
                IM.ChangeInstruction(4); //Change to magnet instructions
                IM.OpenPanel();  //open new pannel
                break;
            case 5:
                IM.ChangeInstruction(5); //Change to Sword instructions
                IM.OpenPanel();  //open new pannel
                break;
        }


        if (CM.A_Button.GetStateDown(CM.anyController))
        {
            switch (selectedI)
            {
                case 0:
                    CM.ToLaser();
                    IM.ChangeInstruction(1); //Change to movement instructions
                    break;
                case 1:
                    CM.ToCannon();
                    IM.ChangeInstruction(1); //Change to movement instructions
                    break;
                case 2:
                    CM.ToMinigun();
                    IM.ChangeInstruction(1);
                    break;
                case 3:
                    CM.ToElectroMagnet();
                    IM.ChangeInstruction(1); //Change to movement instructions
                    break;
                case 4:
                    CM.ToBlackHoleMagnet();
                    IM.ChangeInstruction(1); //Change to movement instructions
                    break;
                case 5:
                    CM.ToSword();
                    IM.ChangeInstruction(1); //Change to movement instructions
                    break;
            }
        }
    }
}
