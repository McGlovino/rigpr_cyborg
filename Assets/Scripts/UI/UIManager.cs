﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject BootText;
    public bool BootTextOn;
    public GameObject Keyboard;
    public bool KeyboardOn;
    public GameObject ArmMountedMenu;
    public bool AMMOn;

    public GameObject startTargetSpawnButton;
    public GameObject magnetGameButton;
    public GameObject swordGameButton;
    public float time = 0;
    public GameObject scoreText;
    public GameObject totalText;
    public GameObject timeText;

    public InstructionsManager IM;
    public ControllsManager CM;
    public TouchpadMovement TM;

    int game = 0;
    public SpawnTargets ST;
    public MagnetMinigame MM;
    public SpawnFruit SF;

    public LeaderboardManager LM;
    public EndGame EG;


    void Start()
    {
        BootText.GetComponent<BootText>().PopulateBootText();
        LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();
        IM.Setup();

        //GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().OpenDoors(); //TURN THIS OFF WHEN TESTING IS DONE

        //CloseBootText();
        OpenBootText();
        CloseAMM();
        //OpenAMM();
        CloseKeyboard();
        //OpenKeyboard();
        CloseInstructions();
        //OpenInstructions();
    }

    void Update()
    {
        if (IM.enabled)
        {
            if (CM.Jostick_Click.GetLastStateDown(CM.rightController))
            {
                OpenInstructions();
            }
        }
        if (startTargetSpawnButton.GetComponent<Button>().pressed == true)
        {
            GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().CloseDoors();
            scoreText.SetActive(true);
            timeText.SetActive(true);
            ST.startGame = true;
            startTargetSpawnButton.GetComponent<Button>().pressed = false;
            startTargetSpawnButton.GetComponent<Button>().Destroy();
            game = 1;
        }
        if (magnetGameButton.GetComponent<Button>().pressed == true)
        {
            GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().CloseDoors();
            scoreText.SetActive(true);
            timeText.SetActive(true);
            MM.SetUp();
            magnetGameButton.GetComponent<Button>().pressed = false;
            magnetGameButton.GetComponent<Button>().Destroy();
            game = 2;
        }
        if (swordGameButton.GetComponent<Button>().pressed == true)
        {
            GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().CloseDoors();
            scoreText.SetActive(true);
            timeText.SetActive(true);
            SF.startGame = true;
            swordGameButton.GetComponent<Button>().pressed = false;
            swordGameButton.GetComponent<Button>().Destroy();
            game = 3;
        }
        if (scoreText.activeSelf)
        {
            switch (game)
            {
                case 1:
                    scoreText.GetComponent<TextMesh>().text = "Score: " + LM.gunScore;
                    break;
                case 2:
                    scoreText.GetComponent<TextMesh>().text = "Score: " + LM.magnetScore;
                    break;
                case 3:
                    scoreText.GetComponent<TextMesh>().text = "Score: " + LM.swordScore;
                    break;
                default:
                    break;
            }
            if(!totalText.activeSelf)//Total text is active when score starts counting down
                LM.UpdatePlayerScore();
        }
        if (totalText.activeSelf)
            totalText.GetComponent<TextMesh>().text = "Total: " + LM.totalScore;
        if (timeText.activeSelf)
            timeText.GetComponent<TextMesh>().text = "Time: " + Mathf.RoundToInt(time);
    }

    public void OpenBootText()
    {
        BootText.SetActive(true);
        BootTextOn = true;
        TM.walkAllowed = false;
    }
    public void CloseBootText()
    {
        BootText.SetActive(false);
        BootTextOn = false;
    }
    public void OpenKeyboard() {
        IM.enabled = true;
        Keyboard.SetActive(true);
        KeyboardOn = true;
        TM.walkAllowed = false;
    }
    public void CloseKeyboard()
    {
        Keyboard.SetActive(false);
        KeyboardOn = false;
        TM.walkAllowed = true;
    }
    public void OpenAMM()
    {
        ArmMountedMenu.SetActive(true);
        AMMOn = true;
    }
    public void CloseAMM()
    {
        ArmMountedMenu.SetActive(false);
        AMMOn = false;
    }

    public void OpenInstructions()
    {
        IM.enabled = true;
        IM.OpenPanel();
    }
    public void CloseInstructions()
    {
        IM.ClosePanel();
    }

    public IEnumerator UpdateTotalScore()
    {
        totalText.SetActive(true);

        Vector3 originalPos = totalText.transform.localPosition;
        Vector3 originalScale = totalText.transform.localScale;

        Vector3 targetPos = new Vector3(-85, 32, 281);
        Vector3 targetScale = new Vector3(450, 450, 450);

        int scoreTemp = 0;
        switch (game)
        {
            case 1:
                scoreTemp = LM.gunScore;
                break;
            case 2:
                scoreTemp = LM.magnetScore;
                break;
            case 3:
                scoreTemp = LM.swordScore;
                break;
            default:
                break;
        }

        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        while (totalText.transform.localPosition.y < targetPos.y -0.05f 
            && totalText.transform.localScale.y < targetScale.y - 0.05f)
        {
            yield return new WaitForFixedUpdate();
            totalText.transform.localPosition = Vector3.Lerp(totalText.transform.localPosition, targetPos, 0.3f);
            totalText.transform.localScale = Vector3.Lerp(totalText.transform.localScale, targetScale, 0.3f);
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
        }
        bool running = true;
        while (running)
        {
            switch (game)
            {
                case 1:
                    if (LM.gunScore > 0){
                        LM.gunScore -= 1;
                        LM.totalScore += 1;}
                    else if (LM.gunScore < 0){
                        LM.gunScore += 2;
                        LM.totalScore -= 1;}
                    else{
                        scoreText.SetActive(false);
                        running = false;}
                    break;
                case 2:
                    if (LM.magnetScore > 0){
                        LM.magnetScore -= 1;
                        LM.totalScore += 1;}
                    else if (LM.magnetScore < 0){
                        LM.magnetScore += 1;
                        LM.totalScore -= 1;}
                    else{
                        scoreText.SetActive(false);
                        running = false;}
                    break;
                case 3:
                    if (LM.swordScore > 0)
                    {
                        LM.swordScore -= 1;
                        LM.totalScore += 1;
                    }
                    else if (LM.swordScore < 0)
                    {
                        LM.swordScore += 1;
                        LM.totalScore -= 1;
                    }
                    else
                    {
                        scoreText.SetActive(false);
                        running = false;
                    }
                    break;
                default:
                    break;
            }

            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        while (totalText.transform.localPosition != originalPos && totalText.transform.localScale != originalScale)
        {
            totalText.transform.localPosition = Vector3.Lerp(totalText.transform.localPosition, originalPos, 0.3f);
            totalText.transform.localScale = Vector3.Lerp(totalText.transform.localScale, originalScale, 0.3f);
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
        }
        switch (game)
        {
            case 1:
                LM.gunScore = scoreTemp;
                ST.startGame = false;
                ST.enabled = false;
                break;
            case 2:
                LM.magnetScore = scoreTemp;
                MM.startGame = false;
                MM.enabled = false;
                break;
            case 3:
                LM.swordScore = scoreTemp;
                SF.startGame = false;
                SF.enabled = false;
                break;
            default:
                break;
        }
        LM.UpdateLeaderboard(game);
        EG.gamesPlayed[game-1] = true;
        game = 0;
        GameObject.FindGameObjectWithTag("DoorManager").GetComponent<DoorsController>().OpenDoors();




    }
}
