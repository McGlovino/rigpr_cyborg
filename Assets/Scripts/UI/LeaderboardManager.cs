﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardManager : MonoBehaviour
{
    public string playerName;
    string[] all;

    public GameObject[] gunLeaderboard;
    public GameObject gunPlayerStats;
    public int gunScore;
    string gunLeaderboardPath = "Assets/Resources/GunLeaderboard.txt";
    public GameObject[] totalLeaderboardGunRoom;//gun room
    public GameObject totalPlayerStatsGunRoom;

    public GameObject[] magnetLeaderboard;
    public GameObject magnetPlayerStats;
    public int magnetScore;
    string  magnetLeaderboardPath = "Assets/Resources/MagnetLeaderboard.txt";
    public GameObject[] totalLeaderboardMagnetRoom;//magnet room
    public GameObject totalPlayerStatsMagnetRoom;

    public GameObject[] swordLeaderboard;
    public GameObject swordPlayerStats;
    public int swordScore;
    string swordLeaderboardPath = "Assets/Resources/SwordLeaderboard.txt";
    public GameObject[] totalLeaderboardSwordRoom;//sword room
    public GameObject totalPlayerStatsSwordRoom;

    public int totalScore;
    string totalLeaderboardPath = "Assets/Resources/TotalLeaderboard.txt";

    void Start()
    {
        // DO ONCE AT START
        UpdateLeaderboard(0);
    }

    public void UpdateLeaderboard(int gameNo, bool updateTotal = false)
    {
        //temporary storeage of all
        //loop thorugh with i+=2
        //text element = alltemp[i] + alltemp[i+1]

        //TOTAL
        all = ReadWriteLeaderboard.ReadLeaderboard(totalLeaderboardPath, totalScore, playerName, updateTotal ? true : false);
        for (int i = 0; i <= 19; i += 2)
            totalLeaderboardGunRoom[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";
        for (int i = 0; i <= 19; i += 2)
            totalLeaderboardMagnetRoom[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";
        for (int i = 0; i <= 19; i += 2)
            totalLeaderboardSwordRoom[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";

        if (gameNo == 1 || gameNo == 0)
        {
            //GUN
            all = ReadWriteLeaderboard.ReadLeaderboard(gunLeaderboardPath, gunScore, playerName, gameNo == 1 ? true : false);
            for (int i = 0; i <= 19; i += 2)
                gunLeaderboard[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";
        }
        if (gameNo == 2 || gameNo == 0)
        {
            //Magnet
            all = ReadWriteLeaderboard.ReadLeaderboard(magnetLeaderboardPath, magnetScore, playerName, gameNo == 2 ? true : false);
            for (int i = 0; i <= 19; i += 2)
                magnetLeaderboard[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";
        }
        if (gameNo == 3 || gameNo == 0)
        {
            //Sword
            all = ReadWriteLeaderboard.ReadLeaderboard(swordLeaderboardPath, swordScore, playerName, gameNo == 3 ? true : false);
            for (int i = 0; i <= 19; i += 2)
                swordLeaderboard[i / 2].GetComponent<Text>().text = ((i + 2) / 2) + ". " + all[i] + " [ " + all[i + 1] + " ]";
        }

        //DISPLAY YOURS AT BOTTOM
        if (playerName != "")
        {
            gunPlayerStats.GetComponent<Text>().text = playerName + " [ " + gunScore + " ] ";
            magnetPlayerStats.GetComponent<Text>().text = playerName + " [ " + magnetScore + " ] ";
            swordPlayerStats.GetComponent<Text>().text = playerName + " [ " + swordScore + " ] ";
            totalPlayerStatsGunRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";
            totalPlayerStatsMagnetRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";
            totalPlayerStatsSwordRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";
        }
    }
    public void UpdatePlayerScore()
    {
        if (playerName != "")
        {
            gunPlayerStats.GetComponent<Text>().text = playerName + " [ " + gunScore + " ] ";
            totalPlayerStatsGunRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";

            magnetPlayerStats.GetComponent<Text>().text = playerName + " [ " + magnetScore + " ] ";
            totalPlayerStatsMagnetRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";

            swordPlayerStats.GetComponent<Text>().text = playerName + " [ " + swordScore + " ] ";
            totalPlayerStatsSwordRoom.GetComponent<Text>().text = playerName + " [ " + totalScore + " ] ";
        }
    }
}
