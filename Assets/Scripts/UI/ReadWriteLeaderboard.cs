﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;

public class ReadWriteLeaderboard
{
    //Read each pair of lines
    //Put into two arrays in order, one for name, one for score


    //see if its higher than lowest (is added)
    //if is added see where it fits by checking going up, if its higher, until hits a no, if reaches top know its first
    //create new array of string
    //put in items in new order

    public static string[] ReadLeaderboard(string path, int currentScore, string currentPlayerName, bool write = false)
    {
        string[] playerNames = new string[10];
        int[] score = new int[10];
        string[] all = new string[20];

        if (File.Exists(path))
        {
            //READ
            StreamReader reader = new StreamReader(path);
            string[] temp = new string[20];
            temp = reader.ReadToEnd().Split(',');
            reader.Close();

            for (int i = 0; i < temp.Length; i++)
            {
                if (i % 2 == 0)
                    playerNames[i / 2] = temp[i]; //.Replace)("\n", String.Empty
                else
                    score[((i + 1) / 2) - 1] = Int32.Parse(temp[i]);
            }

            //COMPARE
            if (currentScore != 0 || currentPlayerName != "")
            {
                int position = 10;

                int[] scoreTemp = new int[10];
                string[] playerNameTemp = new string[10];

                if (currentScore >= score[score.Length - 1])
                {
                    position--;

                    for (int i = score.Length - 2; i > -1; i--)
                    {
                        if (currentScore < score[i])
                            break;

                        position--;
                    }

                    for (int i = 0; i < score.Length; i++)
                    {
                        if (i < position)
                        {
                            scoreTemp[i] = score[i];
                            playerNameTemp[i] = playerNames[i];
                        }
                        else if (i == position)
                        {
                            scoreTemp[i] = currentScore;
                            playerNameTemp[i] = currentPlayerName;
                        }
                        else
                        {
                            scoreTemp[i] = score[i - 1];
                            playerNameTemp[i] = playerNames[i - 1];
                        }
                    }

                    score = scoreTemp;
                    playerNames = playerNameTemp;
                }
            }
            //COMBINE
            for (int i = 0; i < score.Length; i++)
            {
                all[i * 2] = playerNames[i];
                all[(i * 2) + 1] = score[i].ToString();
            }
        }
        else
        {
            write = true;
            all = new string[]{"EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0",
                "EMPTY","0"};
        }
        if (write)
        {
            //WRITE
            using (StreamWriter writer = new StreamWriter(path))
            {
                for (int i = 0; i < all.Length; i++)
                    writer.Write(all[i] + (i == all.Length - 1 ? ' ' : ','));
            }
        }
        return all;
    }

    /*public string[] ReadLeaderboard(string path)
    {
        return ReadString(path, false);
    }
    public string[] UpdateScore(string path, int score, string name)
    {
        string[] all = new string[20];

        ReadString(path, true);
        CompareScore();
        CombineIntoAll();
        WriteString();

        return all;
    }

    void CompareScore(int currentScore, int[] score, string[] playerNames, string currentPlayerName)
    {
        int position = 10;

        int[] scoreTemp = new int[10];
        string[] playerNameTemp = new string[10];

        if (currentScore < score[score.Length - 1])
            return;

        position--;

        for (int i = score.Length - 2; i > -1; i--)
        {
            if (currentScore < score[i])
                break;

            position--;
        }
        
        //Debug.Log(playerName[0]);
        //Debug.Log(playerName[5]);
        //Debug.Log(playerName[9]);

        for (int i = 0; i < score.Length; i++)
        {
            if (i < position) {
                scoreTemp[i] = score[i];
                playerNameTemp[i] = playerNames[i];
            }
            else if (i == position){
                scoreTemp[i] = currentScore;
                playerNameTemp[i] = currentPlayerName;
            }
            else{
                scoreTemp[i] = score[i - 1];
                playerNameTemp[i] = playerNames[i - 1];
            }
        }

        score = scoreTemp;
        playerNames = playerNameTemp;

        //foreach (string s in playerName)
        //    Debug.Log(s);
    }

    string[] CombineIntoAll(int[] score, string[] playerNames, string leaderboardPath)
    {
        string[] all = new string[20];
        for(int i = 0; i < score.Length; i++)
        {
            all[i*2] = playerNames[i];
            all[(i*2) + 1] = score[i].ToString();
        }

        //foreach (string s in all)
        //    Debug.Log(s);
        return WriteString(leaderboardPath, all);
    }

    string[] ReadString(string leaderboardPath, bool compare)
    {
        string[] playerNames = new string[10];
        int[] score = new int[10];

        StreamReader reader = new StreamReader(leaderboardPath);
        string[] temp = new string[20];
        temp = reader.ReadToEnd().Split(',');
        reader.Close();

        for (int i = 0; i < temp.Length; i++)
        {
            if (i % 2 == 0)
                playerNames[i / 2] = temp[i]; //.Replace)("\n", String.Empty
            else
                score[((i + 1) / 2) - 1] = Int32.Parse(temp[i]);
        }

        return CombineIntoAll(score, playerNames, leaderboardPath);
    }

    string[] WriteString(string leaderboardPath, string[] all)
    {
        //File.WriteAllLines(path, all);
        //StreamWriter writer = new StreamWriter(path);
        
        //writer.WriteLine(all);
        /*for (int i = 0; i < all.Length; i++)
        {
            Debug.Log(all[i]);
            writer.Write(all[i]);
        }

        using (StreamWriter writer = new StreamWriter(leaderboardPath))
        {
            for (int i = 0; i < all.Length; i++)
            {
                //Debug.Log(all[i]);
                writer.Write(all[i] + (i == all.Length-1 ? ' ' : ','));
            }
        }
        return all;
    }*/
}
