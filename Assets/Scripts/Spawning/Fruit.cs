﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    public enum FruitType { apple, pear, banana, bomb, bad, broke };

    //public GameObject gObject;

    public FruitType fruitType;
    public int score;
    public bool grabbed;
    LeaderboardManager LM;
    MagnetAttachment MA;

    public void Create(FruitType fruitTypeT)
    {
        grabbed = false;
        fruitType = fruitTypeT;
        LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();
        MA = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<MagnetAttachment>();

        switch (fruitType)
        {
            case FruitType.apple:
                score = 10;
                break;
            case FruitType.pear:
                score = 20;
                break;
            case FruitType.banana:
                score = 50;
                break;
            case FruitType.bomb:
                score = -100;
                break;
            case FruitType.broke:
                score = 0;
                break;
            default:
                score = 10;
                break;
        }
        StartCoroutine(DestroyAfterTime());
    }
    public void Destroy(Collider hit)
    {
        LM.swordScore += score;
        GameObject temp;
        switch (fruitType)
        {
            case Fruit.FruitType.apple:
                temp = Instantiate(Resources.Load("Fruit/AppleDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.pear:
                temp = Instantiate(Resources.Load("Fruit/PearDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.banana:
                temp = Instantiate(Resources.Load("Fruit/BananaDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.bomb:
                temp = Instantiate(Resources.Load("Fruit/BombDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            default:
                temp = Instantiate(Resources.Load("Fruit/AppleDestroyed") as GameObject, transform.position, transform.rotation);
                break;
        }       

        temp.transform.GetChild(0).GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity/4;
        temp.transform.GetChild(1).GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity/4;

        //temp.transform.LookAt(hit.GetContact(0).point);
        //temp.transform.LookAt(-hit.transform.forward);

        temp.transform.GetChild(0).GetComponent<Rigidbody>().AddForce(10 * hit.transform.right);
        temp.transform.GetChild(1).GetComponent<Rigidbody>().AddForce(10 * -hit.transform.right);
        GameObject.Destroy(this.gameObject);
    }
    public void Destroy()
    {
        LM.swordScore += score;
        GameObject temp;
        switch (fruitType)
        {
            case Fruit.FruitType.apple:
                temp = Instantiate(Resources.Load("Fruit/AppleDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.pear:
                temp = Instantiate(Resources.Load("Fruit/PearDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.banana:
                temp = Instantiate(Resources.Load("Fruit/BananaDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            case Fruit.FruitType.bomb:
                temp = Instantiate(Resources.Load("Fruit/BombDestroyed") as GameObject, transform.position, transform.rotation);
                break;
            default:
                temp = Instantiate(Resources.Load("Fruit/AppleDestroyed") as GameObject, transform.position, transform.rotation);
                break;
        }

        temp.transform.GetChild(0).GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity / 4;
        temp.transform.GetChild(1).GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity / 4;

        //temp.transform.LookAt(hit.GetContact(0).point);
        //temp.transform.LookAt(-hit.transform.forward);

        temp.transform.GetChild(0).GetComponent<Rigidbody>().AddForce(10 * -temp.transform.right);
        temp.transform.GetChild(1).GetComponent<Rigidbody>().AddForce(10 * temp.transform.right);
        GameObject.Destroy(this.gameObject);
    }
    public void Destroy2()
    {
        GameObject.Destroy(this.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8 || other.gameObject.layer == 12)
            Destroy2();
        if(other.gameObject.GetComponent<Fruit>() != null)
        {
            MA.LetGo();
            Collider temp = other;
            other.GetComponent<Fruit>().Destroy(this.gameObject.GetComponent<SphereCollider>());
            Destroy(temp);
        }
    }

    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(10);
        GameObject.Destroy(this.gameObject);
    }
}
