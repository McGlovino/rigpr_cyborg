﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public enum Rarity { common, quick, tank, legendary, negativeSlow, negativeFast, bad };

    //public GameObject gObject;
    public Vector3 spawnedAt, goTo;
    public Rarity rarity;
    public bool reachedGoTo;
    public float speed;
    public int hp;
    public int score;
    public HealthBar HB;
    public SpawnTargets ST;
    public bool grabbed;
    LeaderboardManager LM;

    public void Create(Vector3 spawnedAtT, Vector3 goToT, Rarity rarityT, SpawnTargets STT)
    {
        grabbed = false;
        //gObject = gObjectT;
        spawnedAt = spawnedAtT;
        goTo = goToT;
        rarity = rarityT;
        reachedGoTo = false;
        ST = STT;
        LM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<LeaderboardManager>();

        switch (rarity)
        {
            case Rarity.common:
                speed = 120f;
                hp = 4;
                score = 20;
                break;
            case Rarity.quick:
                speed = 220;
                hp = 4;
                score = 40;
                break;
            case Rarity.tank:
                speed = 120f;
                hp = 8;
                score = 40;
                break;
            case Rarity.legendary:
                speed = 220;
                hp = 8;
                score = 100;
                break;
            case Rarity.negativeSlow:
                speed = 120f;
                hp = 1;
                score = -20;
                break;
            case Rarity.negativeFast:
                speed = 220;
                hp = 1;
                score = -20;
                break;
            default:
                speed = 120f;
                hp = 4;
                score = 100;
                break;
        }
        HB = this.gameObject.GetComponentInChildren<HealthBar>();
        HB.Create(hp);

    }
    private void Update()
    {
        HB.updateHealth(hp);
    }

    public int TakeDamage(int damage)
    {
        hp -= damage;
        HB.resetTimer();

        if (hp <= 0)
            Destroy();

        return hp;
    }

    public void Destroy()
    {
        LM.gunScore += score;
        ST.targets.Remove(this);
        GameObject temp = Instantiate(Resources.Load("Destroyed Droid") as GameObject, transform.position, transform.rotation);
        for (int i = 0; i < temp.transform.GetChild(0).childCount; i++){
            temp.transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().material = this.transform.GetChild(0).GetComponent<MeshRenderer>().material;
            temp.transform.GetChild(0).GetChild(i).GetComponent<Rigidbody>().AddExplosionForce(Random.Range(100, 200), transform.position, 50f);
        }
        GameObject.Destroy(this.gameObject);
    }
    public void Destroy2()
    {
        GameObject.Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision other) //very small timer on when it collides
    {
        if (grabbed) {
            TakeDamage(1);
            if (other.transform.tag == "Destroyable")
                ST.FindTarget(other.gameObject).TakeDamage(1);
        }
    }
}
