﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTargets : MonoBehaviour
{

    public bool startGame = false;


    float counter, totalCounter = 0;

    public List<Target> targets;
    public List<GameObject> spawnPoints;
    int spawnTarget;
    float time = 10;//120;
    float spawnAmount = 70;
    float spawnEvery;
    public List<Target.Rarity> rarityRate;
    public List<Material> materials;

    List<float> rarities;
    List<float> spawned;

    public UIManager UIM;

    void Start()
    {
        UIM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<UIManager>();
        targets = new List<Target>();
        spawnEvery = (time / spawnAmount) - 0.1f;
        rarityRate = new List<Target.Rarity>();
        populateRarityRate();
    }

    void populateRarityRate()
    {
        /*commonRarity = 33.33f;
        quickRarity = 16.66f;
        tankRarity = 16.66f;
        legendaryRarity = 8.33f;
        negativeSlowRarity = 16.66f;
        negativeFastRarity = 8.33f;*/
        rarities = new List<float>();
        rarities.Add(36f);
        rarities.Add(16f);
        rarities.Add(16f);
        rarities.Add(8f);
        rarities.Add(16f);
        rarities.Add(8f);

        spawned = new List<float>();
        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);

        rarityRate.Add(Target.Rarity.common);
        rarityRate.Add(Target.Rarity.common);
        rarityRate.Add(Target.Rarity.common);
        rarityRate.Add(Target.Rarity.common);
        rarityRate.Add(Target.Rarity.quick);
        rarityRate.Add(Target.Rarity.quick);
        rarityRate.Add(Target.Rarity.tank);
        rarityRate.Add(Target.Rarity.tank);
        rarityRate.Add(Target.Rarity.legendary);
        rarityRate.Add(Target.Rarity.negativeSlow);
        rarityRate.Add(Target.Rarity.negativeSlow);
        rarityRate.Add(Target.Rarity.negativeFast);
    }

    void Update()
    {
        if (startGame)
        {
            UIM.time = time - totalCounter + 10;
            if (counter >= spawnEvery && totalCounter <= time)
            {
                spawnTarget = Random.Range(0, spawnPoints.Count);
                //Vector3 spawnAt = spawnPoints[spawnTarget].transform.position;
                //spawnPoints[spawnTarget].GetComponent<Animator>().Play("Open", 0);
                spawnPoints[spawnTarget].GetComponentInParent<Animator>().SetTrigger("Open_Trigger");
                StartCoroutine(Spawn(spawnPoints[spawnTarget].transform));
                counter = 0;
               
            }
            else if (totalCounter > time + 10)
            {
                //CM.scoreText.SetActive(false);
                startGame = false;
                UIM.timeText.SetActive(false);
                StartCoroutine(UIM.UpdateTotalScore());
                foreach (Target t in targets)
                    t.Destroy2();
                targets.Clear();
            }
            counter += Time.deltaTime;
            totalCounter += Time.deltaTime;
            moveToGoTo();
        }
    }

    Target.Rarity getRarity(int counter)
    {
        if (counter > 15)
            return Target.Rarity.bad;
        Target.Rarity rarity = rarityRate[Random.Range(0, rarityRate.Count)];
        switch (rarity)
        {
            case Target.Rarity.common:
                if ((spawned[0] / spawnAmount) *100 >= rarities[0]) 
                    rarity = getRarity(counter+1);
                break;
            case Target.Rarity.quick:
                if ((spawned[1] / spawnAmount) * 100 >= rarities[1])
                    rarity = getRarity(counter + 1);
                break;
            case Target.Rarity.tank:
                if ((spawned[2] / spawnAmount) *100 >= rarities[2])
                    rarity = getRarity(counter + 1);
                break;
            case Target.Rarity.legendary:
                if ((spawned[3] / spawnAmount) *100 >= rarities[3])
                    rarity = getRarity(counter + 1);
                break;
            case Target.Rarity.negativeSlow:
                if ((spawned[4] / spawnAmount) *100 >= rarities[4])
                    rarity = getRarity(counter + 1);
                break;
            case Target.Rarity.negativeFast:
                if ((spawned[5] / spawnAmount) *100 >= rarities[5])
                    rarity = getRarity(counter + 1);
                break;
            default:
                if ((spawned[0] / spawnAmount) *100 >= rarities[0])
                    rarity = getRarity(counter + 1);
                break;

        }
        return rarity;
    }
    IEnumerator Spawn(Transform spawnAt)
    {
        Target.Rarity rarity = getRarity(0);
        float waitTime = 0;
        switch (rarity)
        {
            case Target.Rarity.bad:
                yield break;
            case Target.Rarity.common:
                spawned[0]++;
                waitTime = 1.2f;
                break;
            case Target.Rarity.quick:
                spawned[1]++;
                waitTime = 1.5f;
                break;
            case Target.Rarity.tank:
                spawned[2]++;
                waitTime = 1.2f;
                break;
            case Target.Rarity.legendary:
                spawned[3]++;
                waitTime = 1.5f;
                break;
            case Target.Rarity.negativeSlow:
                spawned[4]++;
                waitTime = 1.2f;
                break;
            case Target.Rarity.negativeFast:
                spawned[5]++;
                waitTime = 1.5f;
                break;
            default:
                spawned[0]++;
                waitTime = 1.2f;
                break;
        }
        yield return new WaitForSeconds(waitTime);
        targets.Add((Instantiate(Resources.Load("Droid2") as GameObject, spawnAt.position, spawnAt.rotation)).GetComponent<Target>());
        targets[targets.Count - 1].Create(spawnAt.position, RandomLocation(), rarity, this);
        switch (rarity)
        {
            case Target.Rarity.common:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[0];
                break;
            case Target.Rarity.quick:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[1];
                break;
            case Target.Rarity.tank:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[2];
                break;
            case Target.Rarity.legendary:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[3];
                break;
            case Target.Rarity.negativeSlow:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[4];
                break;
            case Target.Rarity.negativeFast:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[4];
                break;
            default:
                targets[targets.Count - 1].transform.GetChild(0).GetComponent<MeshRenderer>().material = materials[0];
                break;
        }
        yield break;
    }

    void moveToGoTo()
    {
        for(int i = 0; i < targets.Count; i++)
        {
            //if target hasent reached its goal position
            if (targets[i].gameObject != null && !targets[i].reachedGoTo) {

                //Get direction
                Vector3 direction = (targets[i].goTo - targets[i].gameObject.transform.position).normalized;
                float directionLength = (targets[i].goTo - targets[i].gameObject.transform.position).magnitude;
                Debug.DrawRay(targets[i].gameObject.transform.position, direction * directionLength);

                //change rotation
                targets[i].GetComponent<Rigidbody>().MoveRotation(Quaternion.Slerp(targets[i].GetComponent<Rigidbody>().rotation, Quaternion.LookRotation(direction, Vector3.up), 0.2f));

                //if its closer than 5 units away, start slowing down
                if (directionLength < 3f)
                    targets[i].GetComponent<Rigidbody>().velocity = targets[i].gameObject.transform.forward * targets[i].speed 
                        * ((directionLength / 3) > 0.66f ? (directionLength / 3) : 0.66f) * Time.deltaTime;
                else
                    targets[i].GetComponent<Rigidbody>().velocity = targets[i].gameObject.transform.forward * targets[i].speed * Time.deltaTime;

                //reached position
                if (directionLength < 0.9f)
                    targets[i].reachedGoTo = true;
            }
            if (targets[i].reachedGoTo){
                targets[i].reachedGoTo = false;
                targets[i].goTo = RandomLocation();
            }
        }
    }

    private Vector3 RandomLocation()
    {
        return new Vector3(Random.Range(transform.position.x - (transform.lossyScale.x/ 2f), transform.position.x + (transform.lossyScale.x / 2f)),
                Random.Range(transform.position.y - (transform.lossyScale.y / 2f), transform.position.y + (transform.lossyScale.y / 2f)),
                Random.Range(transform.position.z - (transform.lossyScale.z / 2f), transform.position.z + (transform.lossyScale.z / 2f)));
    }


    public Target FindTarget(GameObject g)
    {
        foreach(Target t in targets)
        {
            if (t.gameObject == g)
                return t;
        }
        return null;
    }
}
