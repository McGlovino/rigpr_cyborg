﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFruit : MonoBehaviour
{
    public GameObject targetArea;
    List<Transform> spawnPoints;
    float force = 255; //175;255;

    public bool startGame;
    public UIManager UIM;

    float counter, totalCounter = 0;
    float time = 85;//85
    float spawnAmount = 100;
    float spawnEvery;
    float spawnTimeTemp = 0;

    List<int> spawned;
    List<Fruit.FruitType> fruitTypeRate;
    List<float> fruitRarity;

    void Start()
    {
        spawned = new List<int>();
        fruitTypeRate = new List<Fruit.FruitType>();
        fruitRarity = new List<float>();
        PopulateFruitTypeRate();
        startGame = false;
        spawnEvery = (time / spawnAmount) - 0.1f;

        spawnPoints = new List<Transform>();
        for(int i = 0; i  < this.transform.childCount; i++)
        {
            spawnPoints.Add(this.transform.GetChild(i));
        }
    }

    void PopulateFruitTypeRate()
    {
        fruitTypeRate.Add(Fruit.FruitType.apple);
        fruitTypeRate.Add(Fruit.FruitType.apple);
        fruitTypeRate.Add(Fruit.FruitType.apple);
        fruitTypeRate.Add(Fruit.FruitType.pear);
        fruitTypeRate.Add(Fruit.FruitType.pear);
        fruitTypeRate.Add(Fruit.FruitType.banana);
        fruitTypeRate.Add(Fruit.FruitType.bomb);

        fruitRarity.Add((3f / (float)fruitTypeRate.Count) * 100f);
        fruitRarity.Add((2f / (float)fruitTypeRate.Count) * 100f);
        fruitRarity.Add((1f / (float)fruitTypeRate.Count) * 100f);
        fruitRarity.Add((1f / (float)fruitTypeRate.Count) * 100f);

        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);
        spawned.Add(0);
    }

    void Update()
    {
        UIM.time = time - totalCounter + 5;
        if (counter >= spawnTimeTemp && totalCounter <= time && startGame)
        {

            StartCoroutine(Spawn());
            spawnTimeTemp = spawnEvery + Random.Range(-spawnEvery / 2, spawnEvery / 2);
            counter = 0;
        }
        else if (totalCounter > time + 5 && startGame)
        {
            startGame = false;
            UIM.timeText.SetActive(false);
            StartCoroutine(UIM.UpdateTotalScore());
        }
        counter += Time.deltaTime;
        totalCounter += Time.deltaTime;
    }

    IEnumerator Spawn()
    {
        Fruit.FruitType fruitType = getFruitType(0);
        float waitTime = 0;
        switch (fruitType)
        {
            case Fruit.FruitType.bad:
                Debug.Log("HIT");
                yield break;
            case Fruit.FruitType.apple:
                waitTime = 1.2f;
                break;
            case Fruit.FruitType.pear:
                waitTime = 1.2f;
                break;
            case Fruit.FruitType.banana:
                waitTime = 1.2f;
                break;
            case Fruit.FruitType.bomb:
                waitTime = 1.2f;
                break;
            default:
                waitTime = 1.2f;
                break;
        }
        yield return new WaitForSeconds(waitTime);
        int spawnPoint = Random.Range(0, spawnPoints.Count);
        GameObject projectile;
        switch (fruitType)
        {
            case Fruit.FruitType.bad:
                yield break;
            case Fruit.FruitType.apple:
                projectile = Instantiate((GameObject)Resources.Load("Fruit/Apple"), spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation);
                spawned[0]++;
                break;
            case Fruit.FruitType.pear:
                projectile = Instantiate((GameObject)Resources.Load("Fruit/Pear"), spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation);
                spawned[1]++;
                break;
            case Fruit.FruitType.banana:
                projectile = Instantiate((GameObject)Resources.Load("Fruit/Banana"), spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation);
                spawned[2]++;
                break;
            case Fruit.FruitType.bomb:
                projectile = Instantiate((GameObject)Resources.Load("Fruit/Bomb"), spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation);
                spawned[3]++;
                break;
            default:
                projectile = Instantiate((GameObject)Resources.Load("Fruit/Apple"), spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation);
                spawned[0]++;
                break;
        }
        projectile.GetComponent<Fruit>().Create(fruitType);
        Vector3 randomPoint = targetArea.transform.position;
        randomPoint = randomPoint + new Vector3(Random.Range(-targetArea.transform.localScale.x / 4f, targetArea.transform.localScale.x / 4f)
            , 25, Random.Range(-targetArea.transform.localScale.x / 4f, targetArea.transform.localScale.x / 4f));
        projectile.GetComponent<Rigidbody>().AddForce((randomPoint - projectile.transform.position).normalized * force);
        yield break;
    }

    Fruit.FruitType getFruitType(int reccur)
    {
        if (reccur > 15)
            return Fruit.FruitType.bad;
        Fruit.FruitType fruitType = fruitTypeRate[Random.Range(0, fruitTypeRate.Count)];

        switch (fruitType)
        {
            case Fruit.FruitType.apple:
                if ((spawned[0] / spawnAmount) * 100 >= fruitRarity[0])
                    fruitType = getFruitType(reccur + 1);
                break;
            case Fruit.FruitType.pear:
                if ((spawned[1] / spawnAmount) * 100 >= fruitRarity[1])
                    fruitType = getFruitType(reccur + 1);
                break;
            case Fruit.FruitType.banana:
                if ((spawned[2] / spawnAmount) * 100 >= fruitRarity[2])
                    fruitType = getFruitType(reccur + 1);
                break;
            case Fruit.FruitType.bomb:
                if ((spawned[3] / spawnAmount) * 100 >= fruitRarity[3])
                    fruitType = getFruitType(reccur + 1);
                break;
            default:
                if ((spawned[0] / spawnAmount) * 100 >= fruitRarity[0])
                    fruitType = getFruitType(reccur + 1);
                break;
        }
        return fruitType;
    }
}
