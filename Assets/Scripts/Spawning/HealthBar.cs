﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public bool isActive, isHover;
    int maxHealth, currentHealth;

    public Image healthBar;
    public GameObject allBar;

    float counter;
    float healtBarTime = 1.5f;

    private void Start()
    {
        //Create(10);
    }

    void Update()
    {
        if (isActive || isHover)
        {
            allBar.SetActive(true);
            transform.LookAt(Camera.main.transform.position);
            float currentNew = (((currentHealth - 0) * (25 - 0)) / (maxHealth - 0)) + 0; //NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
            healthBar.rectTransform.sizeDelta = new Vector2(currentNew, 3);
        }
        else
            allBar.SetActive(false);

        counter += Time.deltaTime;
        if (counter >= healtBarTime)
            isActive = false;
    }

    private void LateUpdate()
    {
        isHover = false;
    }
    public void resetTimer()
    {
        isActive = true;
        counter = 0;
    }
    public void updateHealth(int health)
    {
        currentHealth = health;
    }
    public void Create(int health)
    {
        maxHealth = health;
        currentHealth = health;
    }
}
