﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsController : MonoBehaviour
{
    public List<GameObject> doors;
    bool isOpen = false;
    public float doorSpeed;
    public float doorDistance;
    private void Start()
    {
        //OpenDoors();
    }
    public void OpenDoors()
    {
        if (!isOpen)
        {
            StartCoroutine(Open());
            isOpen = true;
        }
    }
    private IEnumerator Open()
    {
        for (int i = 0; i < doorDistance / (doorSpeed/3); i++)
        {
            foreach (GameObject g in doors)
            {
                //Debug.Log(g.name);
                g.transform.GetChild(0).transform.localPosition += new Vector3(doorSpeed/3, 0f, 0);
                g.transform.GetChild(1).transform.localPosition -= new Vector3(doorSpeed/3, 0f, 0);
            }
            yield return new WaitForFixedUpdate();
        }
    }

    public void CloseDoors()
    {
        if (isOpen)
        {
            StartCoroutine(Close());
            isOpen = false;
        }
    }

    private IEnumerator Close()
    {
        for (int i = 0; i < doorDistance / doorSpeed; i++)
        {
            foreach (GameObject g in doors)
            {
                g.transform.GetChild(0).transform.localPosition -= new Vector3(doorSpeed, 0f, 0);
                g.transform.GetChild(1).transform.localPosition += new Vector3(doorSpeed, 0f, 0);
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
