﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class LaserPointer : MonoBehaviour
{
    public ControllsManager CM;
    //References the controller and eleport action
    //public SteamVR_Input_Sources handType;


    public GameObject LaserPrefab; //referemce the laser prefab
    GameObject laser; //laser stores a reference to an instance of the laser
    private Vector3 hitpoint; //The position where the laser hits

    public Transform cameraRigTransfom; //Transform of the camera rig
   // public GameObject teleportReticlePrefab; //Stores a reference to the teleport treticle prefab
   // public GameObject reticle; //A reference to an instace of the reticle
   // public Transform teleprtReticleTransform; //Stores a reference to the teleport for ease of use
    public Transform headTransform; //Stores a referenece to the players head (the camera)
    public Vector3 teleportRetcileOffset; //The reticle offvset from the floor so there is not "Z fighting" with other surfaces
    public LayerMask teleportMask; //A layermask to fliter the areas on which teleports are allowed
    private bool shouldTeleport; //set true when a valid teleport location is found

    public GameObject leftHand;

    Color red = new Color(0.875f,0.1f,0.1f);
    Color green = new Color(0.1f,0.875f,0.1f);
    Gradient greenGrad = new Gradient();
    GradientColorKey[] greenColorKey;
    Gradient redGrad = new Gradient();
    GradientColorKey[] redColorKey;


    bool canTeleport = true;
    public Texture redLaser;
    public Texture greenLaser;

    // Start is called before the first frame update
    void Start()
    {
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[1];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;

        greenColorKey = new GradientColorKey[2];
        greenColorKey[0].color = green;
        greenColorKey[0].time = 0;
        greenColorKey[1].color = Color.white;
        greenColorKey[1].time = 1;
        greenGrad.SetKeys(greenColorKey, alphaKey);

        redColorKey = new GradientColorKey[2];
        redColorKey[0].color = red;
        redColorKey[0].time = 0;
        redColorKey[1].color = Color.white;
        redColorKey[1].time = 1;
        redGrad.SetKeys(redColorKey, alphaKey);

        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();

        laser = Instantiate(LaserPrefab); //spawn a laser and save a reference to it in laser

        laser.transform.SetParent(this.transform);

       // reticle = Instantiate(teleportReticlePrefab); //Spawn a new reticle and save a reference to it

        //teleprtReticleTransform = reticle.transform; //Store the reticles transform component
    }

    // Update is called once per frame
    void Update()
    {
        shouldTeleport = false;

        RaycastHit hit;
        if (CM.Trigger_Touch.GetState(CM.leftController)) //If teleport is activated
        {
            if (!laser.activeSelf)
            {
                laser.SetActive(true);
                //reticle.SetActive(true);
            }


            //Shot a ray from the controller if it hits something store the point it collided and show the laser and hits only places you can teleport to
            if (Physics.Raycast(leftHand.transform.position, leftHand.transform.up, out hit, 100, teleportMask) && canTeleport)
            {
                laser.GetComponent<LineRenderer>().colorGradient = greenGrad;
                //laser.GetComponent<LineRenderer>().material.color = green;
                laser.GetComponent<LineRenderer>().material.mainTexture = greenLaser;
                if (hit.transform.gameObject.layer == 8)
                {

                    hitpoint = hit.point;
                    showAim();

                    //reticle.SetActive(true); //Show reticle taget
                    //teleprtReticleTransform.position = hitpoint + teleportRetcileOffset; //Move the reticle where the ray cast hit with the addition of an offset
                    shouldTeleport = true; //Set should teleport to true to indidcate the script found a valid target
                }
                else
                {
                    laser.GetComponent<LineRenderer>().SetPosition(0, leftHand.transform.position);
                    laser.GetComponent<LineRenderer>().SetPosition(1, leftHand.transform.up * 5000);
                    laser.GetComponent<LineRenderer>().colorGradient = redGrad;
                    laser.GetComponent<LineRenderer>().material.mainTexture = redLaser;
                }
            }
            else
            {
                laser.GetComponent<LineRenderer>().SetPosition(0, leftHand.transform.position);
                laser.GetComponent<LineRenderer>().SetPosition(1, leftHand.transform.up * 5000);
                laser.GetComponent<LineRenderer>().colorGradient = redGrad;
                laser.GetComponent<LineRenderer>().material.mainTexture = redLaser;
            }
        }
        else
        {
            laser.SetActive(false); //hid the laser when the taleport action deactivates
            //reticle.SetActive(false); //Hide reticle when not in use
        }

        if (CM.Trigger_Click.GetStateDown(CM.leftController) && shouldTeleport) //Teleport the player if the button is pressend athere is valid target
        {
            Physics.Raycast(leftHand.transform.position, leftHand.transform.up, out hit, 100, teleportMask);
            if (hit.transform.gameObject.layer == 8)
                Teleport();
        }

    }

    IEnumerator teleportPause()
    {
        canTeleport = false;
        yield return new WaitForSeconds(0.5f);
        canTeleport = true;
    }

    void showAim()
    {
        LineRenderer lr = laser.GetComponent<LineRenderer>();
        RaycastHit hit;
        Physics.Raycast(leftHand.transform.position, leftHand.transform.up, out hit, 100, teleportMask);
        lr.SetPosition(0, leftHand.transform.position);

        if (hit.collider)
            lr.SetPosition(1, hit.point);
        else
            lr.SetPosition(1, leftHand.transform.up * 5000);
    }


    private void Teleport()
    {
        shouldTeleport = false; //Set teleport to false when teleportation is in progress
        //reticle.SetActive(false); //Hide the reticle
        Vector3 difference = cameraRigTransfom.position - headTransform.position; //Calculate the difference between the camera rig and he players head.
        difference.y = 0; //Rest the y position to 0 becasue the calculation doesnt conside the verticle postion of the plaer head
        cameraRigTransfom.position = hitpoint + difference; //Move the camera rig to the postion of he hitpoint and add the calculated difference.
        laser.SetActive(false);
        StartCoroutine(teleportPause());

    }
}
