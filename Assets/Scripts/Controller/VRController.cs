﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRController : MonoBehaviour
{
    public float sensitivity = 0.1f;
    public float maxSpeed = 1.0f;

    public SteamVR_Action_Boolean movePress = null;
    public SteamVR_Action_Vector2 moveValue = null;

    private float speed = 0.0f;

    private CharacterController characterController = null;
    private Transform cameraRig = null;
    private Transform head = null;


    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        cameraRig = SteamVR_Render.Top().origin;
        head = SteamVR_Render.Top().head;
    }

    private void Update()
    {
        HandleHead();
        HandleHeight();
        CalculateMovement();
    }

    private void HandleHead()
    {
        //Store current
        Vector3 oldPos = cameraRig.position;
        Quaternion oldRot = cameraRig.rotation;

        //Rotation
        transform.eulerAngles = new Vector3(0.0f, head.rotation.eulerAngles.y, 0.0f);

        //Restore
        cameraRig.position = oldPos;
        cameraRig.rotation = oldRot;
    }

    private void CalculateMovement()
    {
        //Find Movement Orientation
        Vector3 orientationEuler = new Vector3(0, transform.eulerAngles.y, 0);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        //Quaternion orientation = transform.GetChild(0).rotation;
        Vector3 movement = Vector3.zero;

        //If not moveing
        if (movePress.GetStateUp(SteamVR_Input_Sources.Any))
            speed = 0;

        //If button pressed
        if (movePress.state)
        {
            //Add speed, clamp
            speed += moveValue.axis.y * sensitivity;
            speed = Mathf.Clamp(speed, -maxSpeed * 0.7f, maxSpeed);

            // Orientation
            movement += orientation * (speed * Vector3.forward) * Time.deltaTime;
        }
        //Apply
        characterController.Move(movement);
    }

    private void HandleHeight()
    {
        //Get the head in local space
        float headHeight = Mathf.Clamp(head.localPosition.y, 1, 2);
        characterController.height = headHeight;

        //Cut in half
        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;

        //Move capsile in local space
        newCenter.x = head.localPosition.x;
        newCenter.z = head.localPosition.z;

        //Rotate
        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0) * newCenter;


        //Apply
        characterController.center = newCenter;
    }
}
