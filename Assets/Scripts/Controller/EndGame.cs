﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    float endTime = 600;
    float currentTime = 0;
    public bool[] gamesPlayed;
    bool endGamed = false;

    public GameObject teleportPoint;
    public GameObject player;
    public GameObject assistant;

    public GameObject totalTimeText;

    public GameObject fade;

    public LeaderboardManager LM;

    void Start()
    {
        fade.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        gamesPlayed = new bool[3];
    }

    void Update()
    {
        bool allPlayed = true;
        for (int i = 0; i < gamesPlayed.Length; i++)
        {
            if (gamesPlayed[i] == false)
                allPlayed = false;
        }
        
        if ((currentTime > endTime || allPlayed) && !endGamed) {
            EndTheGame();
            return;
        }

        currentTime += Time.deltaTime;

        totalTimeText.GetComponent<TextMesh>().text = "Total Time Remaining : " + ((int)(endTime - currentTime)).ToString();
    }

    void EndTheGame()
    {
        endGamed = true;
        LM.UpdateLeaderboard(0, true);
        player.transform.position = teleportPoint.transform.position;
        totalTimeText.SetActive(false);
        StartCoroutine(AssistantAttack());
    }

    IEnumerator AssistantAttack()
    {
        while (Mathf.Abs((assistant.transform.position - player.transform.position).magnitude) > 1.0f)
        {
            assistant.transform.position -= (new Vector3(assistant.transform.position.x, player.transform.position.y, assistant.transform.position.z)
                - player.transform.position).normalized * 0.075f;
            assistant.transform.LookAt(player.transform.position);
            yield return new WaitForSeconds(0.0125f);
        }


        //hit anim


        StartCoroutine(FadeToBlack());
    }

    IEnumerator FadeToBlack()
    {
        //slowly fade to black
        float alphaFadeValue = 0;
        while (alphaFadeValue <= 1)
        {
            alphaFadeValue += 0.05f;
            fade.GetComponent<Image>().color = new Color(0, 0, 0, alphaFadeValue);
            yield return new WaitForSeconds(0.05f);
        }
        //wait 10 secs then end game
        yield return new WaitForSeconds(10);
        Application.Quit();
    }
}
