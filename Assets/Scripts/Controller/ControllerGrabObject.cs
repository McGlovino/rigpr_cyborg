﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class ControllerGrabObject : MonoBehaviour
{
    public SteamVR_Input_Sources handType;
    public SteamVR_Behaviour_Pose controllerPose;

    public ControllsManager CM;

    private GameObject collidingObject; // 1
    private GameObject objectInHand; // 2

    public GameObject hand;

    private void Start()
    {
        CM = GameObject.FindGameObjectWithTag("CameraRig").GetComponent<ControllsManager>();
    }

    private void SetCollidingObject(Collider col)
    {
        // 1
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        // 2
        collidingObject = col.gameObject;
    }

    void Update()
    {
        // 1
        if (CM.GrapGrip.GetLastStateDown(handType))
        {

            if (collidingObject && collidingObject.tag == "Grabable")
            {
                //Physics.IgnoreCollision(collidingObject.GetComponent<Collider>(), hand.GetComponent<Collider>(), true);
                GrabObject();
            }
        }

        // 2
        if (CM.GrapGrip.GetLastStateUp(handType))
        {
            if (objectInHand)
            {

                ReleaseObject();
            }
        }

    }

    // 1
    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }

    // 2
    public void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
    }

    // 3
    public void OnTriggerExit(Collider other)
    {
        if (!collidingObject)
        {
            return;
        }

        collidingObject = null;
    }
    private void GrabObject()
    {
        // 1
        objectInHand = collidingObject;
        collidingObject = null;
        // 2
        objectInHand.GetComponent<Rigidbody>().detectCollisions = false;
        //Physics.IgnoreCollision(collidingObject.GetComponent<Collider>(), hand.GetComponent<Collider>(), true);
        //Physics.IgnoreCollision(collidingObject.GetComponent<Collider>(), this.GetComponent<Collider>(), true);
        var joint = AddFixedJoint();
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
    }

    // 3
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    private void ReleaseObject()
    {
        // 1
        if (GetComponent<FixedJoint>())
        {
            // 2
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            // 3

            objectInHand.GetComponent<Rigidbody>().velocity = controllerPose.GetVelocity();
            objectInHand.GetComponent<Rigidbody>().angularVelocity = controllerPose.GetAngularVelocity();

        }
        // 4
        StartCoroutine(detectCollisions(objectInHand.GetComponent<Rigidbody>()));
        //Physics.IgnoreCollision(collidingObject.GetComponent<Collider>(), hand.GetComponent<Collider>(), false);
        //Physics.IgnoreCollision(collidingObject.GetComponent<Collider>(), this.GetComponent<Collider>(), false);
        objectInHand = null;
    }

    IEnumerator detectCollisions(Rigidbody rig)
    {
        yield return new WaitForSeconds(0.1f);
        rig.detectCollisions = true;
    }


}