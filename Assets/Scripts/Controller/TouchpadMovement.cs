﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class TouchpadMovement : MonoBehaviour
{
    public ControllsManager CM;
    public GameObject VrRig;
    public GameObject Camera;

    public bool walkAllowed;

    private float _mMoveSpeed = 2.5f;
    
    void Start()
    {
        CM = this.GetComponent<ControllsManager>();
        walkAllowed = true;
    }
    void Update()
    {
        if (CM.Touchpad.GetActive(CM.leftController) && CM.Touchpad.GetAxis(CM.leftController) != new Vector2(0, 0) && walkAllowed)
        {
            Quaternion orientation = Camera.transform.rotation;
            Vector2 touchPadVector = CM.Touchpad.GetAxis(CM.leftController);
            //touchPadVector.x = 0;
            if (touchPadVector.x < 0.5f && touchPadVector.x > -0.5f)
                touchPadVector.x = 0;
            else
            {
                if (touchPadVector.x > 0)
                    touchPadVector.x -= 0.3f;
                else
                    touchPadVector.x += 0.3f;
            }
            if (touchPadVector.y >0)
                touchPadVector.y *= 2;
            Vector3 moveDirection = (orientation * (Vector3.forward * touchPadVector.y)) + (orientation * (Vector3.right * touchPadVector.x));
            moveDirection = new Vector3(moveDirection.x, 0 , moveDirection.z);
            Debug.DrawRay(new Vector3(Camera.transform.position.x, 0.5f, Camera.transform.position.z), moveDirection * 0.3f);
            if(!Physics.Raycast(new Vector3(Camera.transform.position.x, 0.5f, Camera.transform.position.z), moveDirection, 0.3f)
                || !Physics.Raycast(new Vector3(Camera.transform.position.x, 0.5f, Camera.transform.position.z), moveDirection, 0.2f)
                || !Physics.Raycast(new Vector3(Camera.transform.position.x, 0.5f, Camera.transform.position.z), moveDirection, 0.1f)
                || !Physics.Raycast(new Vector3(Camera.transform.position.x, 0.5f, Camera.transform.position.z), moveDirection, 0.025f))
            {

                Vector3 pos = VrRig.transform.position;
                pos.x += moveDirection.x * _mMoveSpeed * Time.deltaTime;
                pos.z += moveDirection.z * _mMoveSpeed * Time.deltaTime;
                VrRig.transform.position = pos;
            }

        }
    }


}
