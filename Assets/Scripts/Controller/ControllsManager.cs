﻿using System.Collections;
using System.Collections.Generic;
using Valve.VR;
using UnityEngine;

public class ControllsManager : MonoBehaviour
{
    public struct attachment
    {
        public GameObject model;
        public int damage;
        public string name;
        public bool isPalm;
        public attachment(GameObject modelT, int damageT, string nameT, bool isPalmT)
        {
            model = modelT;
            damage = damageT;
            name = nameT;
            isPalm = isPalmT;
        }
        public attachment(int damageT, string nameT, bool isPalmT)
        {
            model = null;
            damage = damageT;
            name = nameT;
            isPalm = isPalmT;
        }
    }

    public SteamVR_Input_Sources leftController = SteamVR_Input_Sources.LeftHand;
    public SteamVR_Input_Sources rightController = SteamVR_Input_Sources.RightHand;
    public SteamVR_Input_Sources anyController = SteamVR_Input_Sources.Any;

    public SteamVR_Action_Vector2 Touchpad;
    public SteamVR_Behaviour_Pose LeftPose;
    public SteamVR_Behaviour_Pose RightPose;
    public SteamVR_Action_Boolean GrapGrip;
    public SteamVR_Action_Boolean Jostick_Click;
    public SteamVR_Action_Vector2 Jostick_Position;
    public SteamVR_Action_Boolean Trigger_Click;
    public SteamVR_Action_Single Trigger_Pull;
    public SteamVR_Action_Boolean Trigger_Touch;
    public SteamVR_Action_Boolean B_Button;
    public SteamVR_Action_Boolean A_Button;

    public List<attachment> attachments = new List<attachment>();
    public attachment currentAtachment;
    public List<GameObject> armObjs;

    public int totalScore = 0;

    public InstructionsManager IM; //maybe remove or change?


    bool menuOn = false;
    public GameObject armMenu;

    public ControllerGrabObject rightControllerGrab;

    private void Start()
    {
        PopulateAttachments();
        ToElectroMagnet();
    }

    private void Update()
    {
        if (this.gameObject.GetComponent<UIManager>().AMMOn)
        {
            if (B_Button.GetStateDown(anyController))
            {
                menuOn = !menuOn;

                IM.ChangeInstruction(1); // change to movement instruction.
            }
            if (menuOn)
            {
                armMenu.GetComponent<ArmMountedMenu>().enabled = true;
                armMenu.SetActive(true);
                if (A_Button.GetStateDown(anyController))
                {
                    menuOn = false;
                }
            }
            else
            {
                armMenu.GetComponent<ArmMountedMenu>().enabled = false;
                armMenu.SetActive(false);
            }
        }
    }

    public void ToLaser()
    {
        rightControllerGrab.enabled = true;
        currentAtachment = attachments[0];
        this.GetComponent<GunAttachment>().enabled = true;
        this.GetComponent<MagnetAttachment>().DisableAttachment();
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 0)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }
    public void ToCannon()
    {
        rightControllerGrab.enabled = false;
        currentAtachment = attachments[1];
        this.GetComponent<GunAttachment>().enabled = true;
        this.GetComponent<MagnetAttachment>().DisableAttachment();
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 1)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }
    public void ToMinigun()
    {
        rightControllerGrab.enabled = false;
        currentAtachment = attachments[2];
        this.GetComponent<GunAttachment>().enabled = true;
        this.GetComponent<MagnetAttachment>().DisableAttachment();
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 5)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }
    public void ToElectroMagnet()
    {
        rightControllerGrab.enabled = true;
        currentAtachment = attachments[3];
        this.GetComponent<GunAttachment>().DisableAttachment();
        this.GetComponent<MagnetAttachment>().enabled = true;
        this.GetComponent<MagnetAttachment>().rightHand = this.GetComponent<MagnetAttachment>().rightHandElectromagnet;
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 2)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }
    public void ToBlackHoleMagnet()
    {
        rightControllerGrab.enabled = false;
        currentAtachment = attachments[4];
        this.GetComponent<GunAttachment>().DisableAttachment();
        this.GetComponent<MagnetAttachment>().enabled = true;
        this.GetComponent<MagnetAttachment>().rightHand = this.GetComponent<MagnetAttachment>().rightHandBlackHole;
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 3)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }
    public void ToSword()
    {
        rightControllerGrab.enabled = true;
        currentAtachment = attachments[5];
        this.GetComponent<GunAttachment>().DisableAttachment();
        this.GetComponent<MagnetAttachment>().DisableAttachment();
        for (int i = 0; i < armObjs.Count; i++)
        {
            if (i == 4)
                armObjs[i].SetActive(true);
            else
                armObjs[i].SetActive(false);
        }
    }

    void PopulateAttachments()
    {
        attachments.Add(new attachment(2, "laser", true));
        attachments.Add(new attachment(4, "cannon", false));
        attachments.Add(new attachment(1, "minigun", false));
        attachments.Add(new attachment(4, "Electromagnet", true));
        attachments.Add(new attachment(4, "BalckHoleMagnet", false));
        attachments.Add(new attachment(4, "Sword", false));

        currentAtachment = attachments[0];
    }    
}
