﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject follow;
    public bool head, atStart;
    Vector3 offset;


    private void Start()
    {
        offset = follow.transform.position - transform.position;

        if (atStart)
        {
            transform.position = follow.transform.position - offset;
        }

        if (head)
        {
            int childCount = transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                transform.position = follow.transform.position;
                transform.rotation = follow.transform.rotation;
                transform.GetChild(0).SetParent(follow.transform);
            }
        }
    }
    void Update()
    {
        if (head && !atStart)
        {
            transform.position = follow.transform.position;
            transform.rotation = follow.transform.rotation;
        }
        else if(!atStart)
        {
            transform.position = follow.transform.position - offset;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, follow.transform.eulerAngles.y, transform.eulerAngles.z);
        }
    }
}
